//
//  CurrencyHelper.swift
//  All In One
//
//  Created by  MacBook on 01/07/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

class CurrencyHelper {
    enum AvailableCurrency: String, CaseIterable {
        case RUB = "RUB"
        case USD = "USD"
        case EUR = "EUR"
    }
    
    static var availableCoins: [(name: String, image: UIImage)] = {
        var availableCurrency = [(name: String, image: UIImage)]()
        availableCurrency.append((name: "BTC", image: #imageLiteral(resourceName: "btc")))
        availableCurrency.append((name: "ETH", image: #imageLiteral(resourceName: "eth")))
        availableCurrency.append((name: "LTC", image: #imageLiteral(resourceName: "ltc")))
        availableCurrency.append((name: "BCH", image: #imageLiteral(resourceName: "bch")))
        availableCurrency.append((name: "XRP", image: #imageLiteral(resourceName: "xrp")))
        availableCurrency.append((name: "BNB", image: #imageLiteral(resourceName: "bnb")))
        availableCurrency.append((name: "BSV", image: #imageLiteral(resourceName: "bsv")))
        return availableCurrency
    }()
    
    static func getImageForCurrancyName(_ currencyName: String) -> UIImage {
        let image =  availableCoins.first(where: { (currency) -> Bool in
            currency.name == currencyName
        })?.image
        return (image != nil ? image! : #imageLiteral(resourceName: "questionMark"))
    }
}
