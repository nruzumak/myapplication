//
//  GlobalExtension.swift
//  All In One
//
//  Created by  MacBook on 28/06/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import Foundation
import UIKit

extension Dictionary {
    
    func sort(isOrderedBefore: (Key, Key) -> Bool) -> [(Key, Value)] {
        var result: [(Key, Value)] = []
        let sortedKeys = keys.sorted(by: isOrderedBefore)
        for key in sortedKeys {
            result.append((key, self[key]!))
        }
        return result
    }
}
