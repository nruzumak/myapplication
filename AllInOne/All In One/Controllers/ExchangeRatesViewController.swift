//
//  ExchangeRatesViewController.swift
//  All In One
//
//  Created by  MacBook on 28/06/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//
import UIKit
import PopMenu
import CoreData

class ExchangeRatesViewController: UIViewController {
    //MARK: Fileds and properties
    @IBOutlet weak var currenciesTableView: UITableView!
    
    private var selectedCurrencyType: CurrencyHelper.AvailableCurrency! {
        didSet {
            currenciesTableView.reloadData()
        }
    }
    private var selectedExchangeRates: APIModels.ExchangeRateOfCoin!
    private var exchangeRateOfCoins: [APIModels.ExchangeRateOfCoin] = [] {
        didSet {
            currenciesTableView.reloadData()
        }
    }
    
    private let currencyCellId = "CurrencyTableViewCell"
    
    private let defaults = UserDefaults.standard
    
    private static let latestExchangeRateToCurrencyEntityName = "ExchangeRateOfCoins"
    private static let currencyNameKey = "name"
    private static let currencyExchangeRateKey = "exchangeRate"
    
    //MARK: Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        configSelf()
        self.getExchangeRateOfCoinsFromStorage()
        self.getExchangeRates()
    }
    
    private func configSelf() {
        let nib = UINib(nibName: currencyCellId, bundle: nil)
        currenciesTableView.register(nib, forCellReuseIdentifier: currencyCellId)
        
        if defaults.string(forKey: "selectedCurrency") != nil, let selectedCurrency = CurrencyHelper.AvailableCurrency(rawValue: defaults.string(forKey: "selectedCurrency")!) {
            selectedCurrencyType = selectedCurrency
        } else {
            setSelectedCurrenct(.RUB)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? SpecificCoinInfoViewController {
            vc.coinAndCurrency = (coin: selectedExchangeRates.name, currency: selectedCurrencyType)
            vc.title = selectedExchangeRates.name
        }
    }
    
    //MARK: - CoreData
    private func saveExchangeRateOfCoins(_ exchangeRates: [APIModels.ExchangeRateOfCoin]) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.managedObjectContext
        
        for exchangeRate in exchangeRates {
            let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: ExchangeRatesViewController.latestExchangeRateToCurrencyEntityName)
            let pred = NSPredicate(format: "%K = %@", ExchangeRatesViewController.currencyNameKey, exchangeRate.name)
            request.predicate = pred
            do {
                let objects = try context.fetch(request)
                var object = objects.first as? NSManagedObject
                if object == nil {
                    object = NSEntityDescription.insertNewObject(forEntityName: ExchangeRatesViewController.latestExchangeRateToCurrencyEntityName, into: context)
                }
                object?.setValue(exchangeRate.name, forKey: ExchangeRatesViewController.currencyNameKey)
                object?.setValue(exchangeRate.exchangeRate, forKey: ExchangeRatesViewController.currencyExchangeRateKey)
                appDelegate.saveContext()
            } catch {
                print(error)
            }
        }
    }
    
    private func getExchangeRateOfCoinsFromStorage() {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.managedObjectContext
        
        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: ExchangeRatesViewController.latestExchangeRateToCurrencyEntityName)
        do {
            let objects = try context.fetch(request)
            var storedExchangeRates : [APIModels.ExchangeRateOfCoin] = []
            for object in objects {
                guard let object = object as? NSManagedObject else {
                    continue
                }
                let storedCurrency = APIModels.ExchangeRateOfCoin(
                    name: object.value(forKey: ExchangeRatesViewController.currencyNameKey) as! String,
                    exchangeRate: object.value(forKey: ExchangeRatesViewController.currencyExchangeRateKey) as! [String : Double]
                )
                storedExchangeRates.append(storedCurrency)
            }
            exchangeRateOfCoins = storedExchangeRates.sorted { $0.name < $1.name }
        } catch {
            print(error)
        }
    }
    
    // MARK: - Requests
    private func getExchangeRates() {
        API.sharedAPIService.getExchangeRateOfCoinsToCurrencies(tsyms: ["RUB", "USD", "EUR"], fsyms: ["BTC", "ETH", "LTC", "BCH", "XRP", "BNB", "BSV"]) { (result) in
            switch result {
            case .success(let data):
                self.exchangeRateOfCoins = data.sorted(by: { (first, second) -> Bool in
                    return first.name < second.name
                })
                self.saveExchangeRateOfCoins(self.exchangeRateOfCoins)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    //MARK: - Methods
    private func setSelectedCurrenct(_ currency: CurrencyHelper.AvailableCurrency) {
        defaults.set(currency.rawValue, forKey: "selectedCurrency")
        selectedCurrencyType = currency
    }
    
    //MARK: - Actions
    @IBAction func changeCurrency(_ sender: Any) {
        let popupMenu = PopMenuViewController(actions: [
            PopMenuDefaultAction(title: "RUB", image: #imageLiteral(resourceName: "currency-ruble"), color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)),
            PopMenuDefaultAction(title: "USD", image: #imageLiteral(resourceName: "currency-dollar"), color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)),
            PopMenuDefaultAction(title: "EUR", image: #imageLiteral(resourceName: "currency-euro"), color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
            ])
        popupMenu.appearance.popMenuColor.backgroundColor = .solid(fill: #colorLiteral(red: 0.1294117647, green: 0.5882352941, blue: 0.9529411765, alpha: 1))
        popupMenu.appearance.popMenuColor.actionColor = .tint(#colorLiteral(red: 0.1294117647, green: 0.5882352941, blue: 0.9529411765, alpha: 1))
        popupMenu.appearance.popMenuItemSeparator = .fill(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), height: 1)
        popupMenu.shouldEnablePanGesture = false
        popupMenu.delegate = self
        self.present(popupMenu, animated: true)
    }
    
}

//MARK: Extensions
extension ExchangeRatesViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exchangeRateOfCoins.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedExchangeRates = exchangeRateOfCoins[indexPath.row]
        performSegue(withIdentifier: "toSpecificCoinInfo", sender: tableView)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: currencyCellId) as! CurrencyTableViewCell
        let exchangeRate = exchangeRateOfCoins[indexPath.row]
        cell.selectedCurrency = selectedCurrencyType
        cell.exchangeRateOfCoin = exchangeRate
        return cell
    }
}

extension ExchangeRatesViewController: PopMenuViewControllerDelegate {
    func popMenuDidSelectItem(_ popMenuViewController: PopMenuViewController, at index: Int) {
        var selectedCurrency: CurrencyHelper.AvailableCurrency
        switch index{
        case 0:
            selectedCurrency = .RUB
        case 1:
            selectedCurrency = .USD
        case 2:
            selectedCurrency = .EUR
        default:
            selectedCurrency = .RUB
        }
        setSelectedCurrenct(selectedCurrency)
    }
    
}
