//
//  SpecificCurrencyRateViewController.swift
//  All In One
//
//  Created by  MacBook on 01/07/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit
import Charts
import SDWebImage

class SpecificCoinInfoViewController: UIViewController {
    private enum RateType {
        case Hour
        case Week
        case Month
        case Year
    }
    
    @IBOutlet weak var chartView: LineChartView!
    @IBOutlet weak var cointPriceLabel: UILabel!
    @IBOutlet weak var coinImageView: UIImageView!
    @IBOutlet weak var totalVolumeLabel: UILabel!
    @IBOutlet weak var supplyLabel: UILabel!
    @IBOutlet weak var dateTypeSegmentedControl: UISegmentedControl!
    
    var coinAndCurrency: (coin: String, currency: CurrencyHelper.AvailableCurrency)!
    var coinInfoData: APIModels.CoinInfoData! {
        didSet {
            if coinInfoData != nil {
                setCoinInfo()
            }
        }
    }
    private var historicalExchangeRate: APIModels.HistoricalExchangeRate! {
        didSet {
            createDataSet(.Month, historicalExchangeRate)
        }
    }
    private var exchangeRateOfCoin : APIModels.ExchangeRateOfCoin! {
        didSet {
            setExchangeRateOfCoin()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configSelf()
        reloadAllData()
    }
    
    private func configSelf() {
        //chartView config
        chartView.xAxis.drawLabelsEnabled = true
        chartView.xAxis.drawGridLinesEnabled = false
        chartView.xAxis.drawAxisLineEnabled = false
        chartView.xAxis.forceLabelsEnabled = true
        chartView.xAxis.avoidFirstLastClippingEnabled = false
        chartView.xAxis.labelPosition = .bottom
        chartView.xAxis.labelRotationAngle = 90
        
        chartView.drawMarkers = true
        let marker = CurrencyMarker(color: #colorLiteral(red: 0, green: 0.568627451, blue: 0.9176470588, alpha: 1), font: UIFont(name: "PingFangHK-Regular", size: 14)!, textColor: .white, insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 20))
        marker.chartView = chartView
        chartView.marker = marker
        marker.xAxisMagn = " \(coinAndCurrency!.currency)"
        
        chartView.leftAxis.drawGridLinesEnabled = false
        chartView.leftAxis.drawLabelsEnabled = false
        chartView.leftAxis.drawAxisLineEnabled = false
        
        chartView.rightAxis.drawGridLinesEnabled = false
        chartView.rightAxis.drawLabelsEnabled = false
        chartView.rightAxis.drawAxisLineEnabled = false
        
        chartView.drawGridBackgroundEnabled = false
        chartView.legend.enabled = false
        chartView.doubleTapToZoomEnabled = false
        chartView.pinchZoomEnabled = true
        chartView.setViewPortOffsets(left: 0, top: 0, right: 0, bottom: 0)
        
        let formatter = DateValueFormatter()
        chartView.xAxis.valueFormatter = formatter
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(doubleTapChartView))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.cancelsTouchesInView = false
        chartView.addGestureRecognizer(doubleTap)
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        coinImageView.image = CurrencyHelper.getImageForCurrancyName(coinAndCurrency?.coin ?? "unknown")
    }
    
    private func setCoinInfo() {
        supplyLabel.text = String(Int(coinInfoData.conversionInfo.supply))
        totalVolumeLabel.text = String(Int(coinInfoData.conversionInfo.totalVolume24H))
    }
    
    private func setExchangeRateOfCoin() {
        if coinAndCurrency != nil, let value = exchangeRateOfCoin.exchangeRate[coinAndCurrency!.currency.rawValue] {
            cointPriceLabel.text = "\(Int(value)) \(coinAndCurrency.currency)"
        }
    }
    
    private func createDataSet(_ rateType: RateType, _ exchangeRate: APIModels.HistoricalExchangeRate) {
        let dataSet = LineChartDataSet()
        let sortedData = exchangeRate.data.sorted { (firstData, secondData) -> Bool in
            return firstData.time < secondData.time
        }
        sortedData.forEach { (data) in
            let dataEntry = ChartDataEntry(x: Double(data.time), y: Double(round(100 * data.average)/100))
            dataSet.append(dataEntry)
        }
        dataSet.lineWidth = 3
        dataSet.colors = [#colorLiteral(red: 1, green: 0.3411764706, blue: 0.1333333333, alpha: 1)]
        dataSet.drawValuesEnabled = false
        dataSet.drawCirclesEnabled = false
        dataSet.mode = .horizontalBezier
        dataSet.drawFilledEnabled = true
        dataSet.fillAlpha = 0.25
        dataSet.fillColor = #colorLiteral(red: 1, green: 0.3411764706, blue: 0.1333333333, alpha: 1)
        
        let data = LineChartData(dataSet: dataSet)
        chartView.data = data
    }
    
    //MARK : - API
    private func reloadAllData(showLoadScreen: Bool = false) {
        getHistoricalRate()
        getCurrencyInfo()
        getExchangeRateOfCoin()
    }
    
    private func getHistoricalRate(rateType: RateType = .Month) {
        dateTypeSegmentedControl.isEnabled = false
        API.sharedAPIService.getHistoricalRate(tsym: coinAndCurrency!.currency.rawValue, fsym: coinAndCurrency!.coin, limit: 12, type: .Daily) { (result) in
            switch result {
            case .success(let data):
                self.historicalExchangeRate = data
            case .failure(let error):
                print(error)
            }
            self.dateTypeSegmentedControl.isEnabled = true
        }
    }
    
    private func getExchangeRateOfCoin() {
        let allCurrencies = CurrencyHelper.AvailableCurrency.allCases.map { (currency) -> String in
            return currency.rawValue
        }
        API.sharedAPIService.getExchangeRateOfCoinToCurrency(tsyms: allCurrencies, fsym: coinAndCurrency.coin) { (result) in
            switch result {
            case .success(let data):
                self.exchangeRateOfCoin = data
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func getCurrencyInfo() {
        API.sharedAPIService.getCoinInfo(tsym: coinAndCurrency!.currency.rawValue, fsym: coinAndCurrency!.coin) { (result) in
            switch result {
            case .success(let data):
                self.coinInfoData = data
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @objc func doubleTapChartView(_ sender: Any) {
        chartView.zoom(scaleX: 0, scaleY: 0, x: 0, y: 0)
    }
}

extension SpecificCoinInfoViewController : UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return gestureRecognizer.view?.accessibilityIdentifier != "ChartView"
    }
}

public class DateValueFormatter: NSObject, IAxisValueFormatter {
    private let dateFormatter = DateFormatter()
    var dateFormat: String {
        didSet {
            dateFormatter.dateFormat = dateFormat
        }
    }
    
    override init() {
        self.dateFormat = "dd LLLL"
        self.dateFormatter.dateFormat = self.dateFormat
        super.init()
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let date = Date(timeIntervalSince1970: value)
        let stringForValue = dateFormatter.string(from: date)
        return stringForValue
    }
}

