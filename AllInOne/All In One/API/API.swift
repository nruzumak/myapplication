//
//  API.swift
//  All In One
//
//  Created by  MacBook on 28/06/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

enum Result<Value> {
    case success(Value)
    case failure(Error)
}

struct ConnectionSetting {
    static let sheme = "https"
    static let host : String = "min-api.cryptocompare.com"
    static let api_key: String  = "0efc8420780ea845761828a3bfb4078fa2242c8bf553ae06b2f31b1cef6a99e3"
    static var fullUrl : String {
        get {
            let fullUrl = sheme + host
            return fullUrl
        }
    }
}

class API {
    static let sharedAPIService = API()
    
    func sendGet(parameters : [String: Any]?, path: String?, completion: ((Result<Data>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = ConnectionSetting.sheme
        urlComponents.host = ConnectionSetting.host
        urlComponents.path = "/\(path ?? "")"
        var urlParameters = [URLQueryItem]()
        urlParameters.append(URLQueryItem(name: "api_key", value: ConnectionSetting.api_key))
        parameters?.forEach { keyValue in
            if let array = keyValue.value as? [String] {
                urlParameters.append(URLQueryItem(name: keyValue.key, value: String(describing: array.joined(separator: ","))))
            } else {
                urlParameters.append(URLQueryItem(name: keyValue.key, value: String(describing: keyValue.value)))
            }
        }
        
        urlComponents.queryItems = urlParameters
        
        guard let url = urlComponents.url else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Could not create URL from components"]) as Error
            completion?(.failure(error))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 30.0
        config.timeoutIntervalForResource = 30.0
        let session = URLSession(configuration: config)
        
        #if DEBUG
        print("GET on \(url.absoluteString) with parameters=\(urlComponents.query ?? "")")
        #endif
        
        let task = session.dataTask(with: request) { (responseData, response, responseError) in DispatchQueue.main.async {
            if let error = responseError {
                completion?(.failure(error))
            } else if let jsonData = responseData {
                let stringData = String(bytes: jsonData, encoding: .utf8) ?? "Cant decode to string"
                #if DEBUG
                print("jsonData from \(url.absoluteString) = \(stringData)")
                #endif
                completion?(.success(jsonData))
            } else {
                let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data was not retrieved from request"]) as Error
                completion?(.failure(error))
            }
            }
        }
        task.resume()
    }
    
    func test() -> Bool {
        
    }
    
    func getExchangeRateOfCoinToCurrency(tsyms: [String], fsym: String, completion: ((Result<APIModels.ExchangeRateOfCoin>) -> Void)?) {
        var parameters: [String : Any] = [:]
        parameters["tsyms"] = tsyms
        parameters["fsym"] = fsym
        sendGet(parameters: parameters, path: "data/price") { (result) in
            switch result {
            case .success(let data):
                do {
                    let responseDict: [String : Double] = try self.decodeDataToJson(data: data)
                    let response = APIModels.ExchangeRateOfCoin(name: fsym, exchangeRate: responseDict)
                    completion?(.success(response))
                } catch {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    func getExchangeRateOfCoinsToCurrencies(tsyms: [String], fsyms: [String], completion: ((Result<[APIModels.ExchangeRateOfCoin]>) -> Void)?) {
        var parameters: [String : Any] = [:]
        parameters["tsyms"] = tsyms
        parameters["fsyms"] = fsyms
        
        sendGet(parameters: parameters, path: "data/pricemulti") { (result) in
            switch result {
            case .success(let data):
                do {
                    let responseDict: [String : [String : Double]] = try self.decodeDataToJson(data: data)
                    let response = responseDict.map { (keyValue) in
                        return APIModels.ExchangeRateOfCoin(name: keyValue.key, exchangeRate: keyValue.value)
                    }
                    completion?(.success(response))
                } catch {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    func getCoinsInfo(tsym: String, fsyms: [String], completion: ((Result<[APIModels.CoinInfoData]>) -> Void)?) {
        var parameters: [String : Any] = [:]
        parameters["tsym"] = tsym
        parameters["fsyms"] = fsyms
        
        sendGet(parameters: parameters, path: "data/coin/generalinfo") { (result) in
            switch result {
            case .success(let data):
                do {
                    let responseResult: APIModels.CoinInfoResult = try self.decodeDataToJson(data: data)
                    let response = responseResult.data
                    completion?(.success(response))
                } catch {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    func getCoinInfo(tsym: String, fsym: String, completion: ((Result<APIModels.CoinInfoData>) -> Void)?) {
        var parameters: [String : Any] = [:]
        parameters["tsym"] = tsym
        parameters["fsyms"] = fsym
        
        sendGet(parameters: parameters, path: "data/coin/generalinfo") { (result) in
            switch result {
            case .success(let data):
                do {
                    let responseResult: APIModels.CoinInfoResult = try self.decodeDataToJson(data: data)
                    guard let response = responseResult.data.first else {
                        completion?(.failure(NSError(domain: "api", code: 0, userInfo: nil)))
                        return
                    }
                    completion?(.success(response))
                } catch {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }

    func getHistoricalRate(tsym: String, fsym: String, aggregate: Int = 1, limit: Int = 1, type: APIModels.HistoricalExchangeRate.TypeHistoricalRate = .Daily, completion: ((Result<APIModels.HistoricalExchangeRate>) -> Void)?) {
        var parameters: [String : Any] = [:]
        parameters["tsym"] = tsym
        parameters["fsym"] = fsym
        parameters["aggregate"] = aggregate
        parameters["limit"] = limit
        
        var path = "data/"
        
        switch type {
        case .Minute:
            path += "histominute"
        case .Hourly:
            path += "histohour"
        case .Daily:
            path += "histoday"
        }
        
        sendGet(parameters: parameters, path: path) { (result) in
            switch result {
            case .success(let data):
                do {
                    let response: APIModels.HistoricalExchangeRate = try self.decodeDataToJson(data: data)
                    completion?(.success(response))
                } catch {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    private func decodeDataToJson<T: Codable>(data: Data) throws -> T {
        let response = try JSONDecoder().decode(T.self, from: data)
        return response
    }
}

