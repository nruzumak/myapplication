//
//  CurrencyTableViewCell.swift
//  All In One
//
//  Created by  MacBook on 28/06/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {
    var exchangeRateOfCoin : APIModels.ExchangeRateOfCoin! {
        didSet {
            if exchangeRateOfCoin != nil {
                setInfo()
            }
        }
    }
    var selectedCurrency: CurrencyHelper.AvailableCurrency!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var currancyImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    private func setInfo() {
        nameLabel.text = exchangeRateOfCoin.name
        if let price = exchangeRateOfCoin.exchangeRate[selectedCurrency!.rawValue] {
            valueLabel.text = String(price) + " \(selectedCurrency.rawValue)"
        } else {
            valueLabel.text = nil
        }
        currancyImage.image = CurrencyHelper.getImageForCurrancyName(exchangeRateOfCoin.name)
    }
}
