//
//  BalloonMarker.swift
//  All In One
//
//  Created by  MacBook on 02/07/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import Charts

class CurrencyMarker: MarkerImage
{
    enum TriangleType {
        case Less
        case More
        case Equal
        case Empty
    }
    open var color: UIColor
    open var arrowSize = CGSize(width: 15, height: 11)
    open var font: UIFont
    open var textColor: UIColor
    open var insets: UIEdgeInsets
    open var spaceTriangle: CGFloat = 10
    open var minimumSize = CGSize()
    open var triangleSize = CGSize(width: 12, height: 12)
    open var xAxisMagn = ""
    
    fileprivate var _valueLabel: String?
    fileprivate var _valueLabelSize: CGSize = CGSize()
    fileprivate var _paragraphStyle: NSMutableParagraphStyle?
    fileprivate var _drawAttributes = [NSAttributedString.Key : Any]()
    fileprivate var triangleType = TriangleType.Empty
    
    public init(color: UIColor, font: UIFont, textColor: UIColor, insets: UIEdgeInsets)
    {
        self.color = color
        self.font = font
        self.textColor = textColor
        self.insets = insets
        
        _paragraphStyle = NSParagraphStyle.default.mutableCopy() as? NSMutableParagraphStyle
        _paragraphStyle?.alignment = .center
        super.init()
    }
    
    open override func offsetForDrawing(atPoint point: CGPoint) -> CGPoint
    {
        var offset = self.offset
        var size = self.size
        
        if size.width == 0.0 && image != nil
        {
            size.width = image!.size.width
        }
        if size.height == 0.0 && image != nil
        {
            size.height = image!.size.height
        }
        
        let width = size.width
        let height = size.height
        let padding: CGFloat = 8.0
        
        var origin = point
        origin.x -= width / 2
        origin.y -= height
        
        if origin.x + offset.x < 0.0
        {
            offset.x = -origin.x + padding
        } else if let chart = chartView, origin.x + width + offset.x > chart.bounds.size.width {
            offset.x = chart.bounds.size.width - origin.x - width - padding
        }
        
        if origin.y + offset.y < 0
        {
            offset.y = height + padding;
        }
        else if let chart = chartView,
            origin.y + height + offset.y > chart.bounds.size.height
        {
            offset.y = chart.bounds.size.height - origin.y - height - padding
        }
        
        return offset
    }
    
    open override func draw(context: CGContext, point: CGPoint)
    {
        guard let label = _valueLabel else { return }
        
        let offset = self.offsetForDrawing(atPoint: point)
        let size = self.size
        
        var rect = CGRect(
            origin: CGPoint(
                x: point.x + offset.x,
                y: point.y + offset.y),
            size: size)
        rect.origin.x -= size.width / 2.0
        rect.origin.y -= size.height
        
        context.saveGState()
        
        context.setFillColor(color.cgColor)
        
        if offset.y > 0
        {
            context.beginPath()
            context.move(to: CGPoint(
                x: rect.origin.x,
                y: rect.origin.y + arrowSize.height))
            context.addLine(to: CGPoint(
                x: rect.origin.x + (rect.size.width - arrowSize.width) / 2.0,
                y: rect.origin.y + arrowSize.height))
            //arrow vertex
            context.addLine(to: CGPoint(
                x: point.x,
                y: point.y))
            context.addLine(to: CGPoint(
                x: rect.origin.x + (rect.size.width + arrowSize.width) / 2.0,
                y: rect.origin.y + arrowSize.height))
            context.addLine(to: CGPoint(
                x: rect.origin.x + rect.size.width,
                y: rect.origin.y + arrowSize.height))
            context.addLine(to: CGPoint(
                x: rect.origin.x + rect.size.width,
                y: rect.origin.y + rect.size.height))
            context.addLine(to: CGPoint(
                x: rect.origin.x,
                y: rect.origin.y + rect.size.height))
            context.addLine(to: CGPoint(
                x: rect.origin.x,
                y: rect.origin.y + arrowSize.height))
            context.fillPath()
        }
        else
        {
            context.beginPath()
            context.move(to: CGPoint(
                x: rect.origin.x,
                y: rect.origin.y))
            context.addLine(to: CGPoint(
                x: rect.origin.x + rect.size.width,
                y: rect.origin.y))
            context.addLine(to: CGPoint(
                x: rect.origin.x + rect.size.width,
                y: rect.origin.y + rect.size.height - arrowSize.height))
            context.addLine(to: CGPoint(
                x: rect.origin.x + (rect.size.width + arrowSize.width) / 2.0,
                y: rect.origin.y + rect.size.height - arrowSize.height))
            //arrow vertex
            context.addLine(to: CGPoint(
                x: point.x,
                y: point.y))
            context.addLine(to: CGPoint(
                x: rect.origin.x + (rect.size.width - arrowSize.width) / 2.0,
                y: rect.origin.y + rect.size.height - arrowSize.height))
            context.addLine(to: CGPoint(
                x: rect.origin.x,
                y: rect.origin.y + rect.size.height - arrowSize.height))
            context.addLine(to: CGPoint(
                x: rect.origin.x,
                y: rect.origin.y))
            context.fillPath()
        }
        
        if offset.y > 0 {
            rect.origin.y += self.insets.top + arrowSize.height
        } else {
            rect.origin.y += self.insets.top
        }
        rect.size.height -= self.insets.top + self.insets.bottom
        
        UIGraphicsPushContext(context)
        
        label.draw(in: rect, withAttributes: _drawAttributes)

        //Create triangle
        context.beginPath()
        switch triangleType {
        case .Less:
            context.move(to: CGPoint(
                x: rect.minX + insets.left,
                y: rect.minY
            ))
            context.addLine(to: CGPoint(
                x: rect.minX + triangleSize.width / 2 + insets.left,
                y: rect.minY + triangleSize.height
            ))
            context.addLine(to: CGPoint(
                x: rect.minX + triangleSize.width + insets.left,
                y: rect.minY
            ))
            
            context.setFillColor(red: 1, green: 0, blue: 0, alpha: 1.0)
        case .More:
            context.move(to: CGPoint(
                x: rect.minX + insets.left + triangleSize.width / 2,
                y: rect.minY
            ))
            context.addLine(to: CGPoint(
                x: rect.minX + insets.left,
                y: rect.minY + triangleSize.height
            ))
            context.addLine(to: CGPoint(
                x: rect.minX + triangleSize.width + insets.left,
                y: rect.minY + triangleSize.height
            ))
            
            context.setFillColor(red: 0, green: 1, blue: 0, alpha: 1.0)
        case .Equal:
            fallthrough
        case .Empty:
            break
        }
        context.fillPath()
        
        UIGraphicsPopContext()
        
        context.restoreGState()
    }
    
    open override func refreshContent(entry: ChartDataEntry, highlight: Highlight)
    {
        setTriangleType(entry: entry, highlight: highlight)
        setValueLabel(entry)
    }
    
    open func setTriangleType(entry: ChartDataEntry, highlight: Highlight) {
        guard let chartData = chartView?.data else {
            triangleType = .Empty
            return
        }
        let dataSet = chartData.dataSets[highlight.dataSetIndex]
        let highlightIndex = dataSet.entryIndex(entry: entry)
        if highlightIndex > 0 {
            guard let prevEntry = dataSet.entryForIndex(highlightIndex - 1) else {
                triangleType = .Empty
                return
            }
            if entry.y > prevEntry.y {
                triangleType = .More
            } else if entry.y < prevEntry.y {
                triangleType = .Less
            } else {
                triangleType = .Equal
            }
        } else {
            triangleType = .Empty
        }
    }
    
    open func setValueLabel(_ entry: ChartDataEntry)
    {
        let xAxis = chartView?.xAxis.valueFormatter?.stringForValue(entry.x, axis: nil) ?? String(entry.x)
        _valueLabel = "\(entry.y) \(xAxisMagn)\n" +
            "\(xAxis)"
        
        _drawAttributes.removeAll()
        _drawAttributes[.font] = self.font
        _drawAttributes[.paragraphStyle] = _paragraphStyle
        _drawAttributes[.foregroundColor] = self.textColor
        
        _valueLabelSize = _valueLabel?.size(withAttributes: _drawAttributes) ?? CGSize.zero
        
        var size = CGSize()
        size.width = _valueLabelSize.width + self.insets.left + self.insets.right + self.triangleSize.width + self.spaceTriangle
        size.height = _valueLabelSize.height + self.insets.top + self.insets.bottom
        size.width = max(minimumSize.width, size.width)
        size.height = max(minimumSize.height, size.height)
        self.size = size
    }
}
