//
//  BalloonMarker.swift
//  All In One
//
//  Created by  MacBook on 02/07/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import Charts

class CurrencyMarker: MarkerImage
{
    enum TriangleType {
        case Less
        case More
        case Equal
        case Empty
    }
    open var color: UIColor
    open var arrowSize = CGSize(width: 15, height: 11)
    open var font: UIFont
    open var textColor: UIColor
    open var insets: UIEdgeInsets
    open var minimumSize = CGSize()
    open var xAxisMagn = ""
    
    fileprivate var triangleSize : CGSize {
        get {
            return CGSize(width: (2 / sqrt(3.0)) * _percentLabelSize.height / 2,
                          height: _percentLabelSize.height / 2)
        }
    }
    fileprivate let _spaceBetweenPercAndValue: CGFloat = 9
    fileprivate(set) var differenceEntryPercent: CGFloat = 0.00
    fileprivate var triangleType = TriangleType.Empty
    
    fileprivate var _percentLabel: String?
    fileprivate var _percentLabelSize: CGSize = CGSize()
    fileprivate var _percentStyle: NSMutableParagraphStyle?
    fileprivate var _percentAttributes = [NSAttributedString.Key : Any]()
    
    fileprivate var _dateAndValueLabel: String?
    fileprivate var _dateAndValueSize: CGSize = CGSize()
    fileprivate var _dateAndValueStyle: NSMutableParagraphStyle?
    fileprivate var _dateAndValueAttributes = [NSAttributedString.Key : Any]()
    
    
    
    public init(color: UIColor, font: UIFont, textColor: UIColor, insets: UIEdgeInsets)
    {
        self.color = color
        self.font = font
        self.textColor = textColor
        self.insets = insets
        
        super.init()
        
        _dateAndValueStyle = NSParagraphStyle.default.mutableCopy() as? NSMutableParagraphStyle
        _dateAndValueStyle?.alignment = .left
        _dateAndValueAttributes.removeAll()
        _dateAndValueAttributes[.font] = self.font
        _dateAndValueAttributes[.paragraphStyle] = _dateAndValueStyle
        _dateAndValueAttributes[.foregroundColor] = self.textColor
        
        _percentStyle = NSParagraphStyle.default.mutableCopy() as? NSMutableParagraphStyle
        _percentStyle?.alignment = .right
        _percentAttributes.removeAll()
        _percentAttributes[.font] = self.font
        _percentAttributes[.paragraphStyle] = _percentStyle
        _percentAttributes[.foregroundColor] = self.textColor
    }
    
    open override func offsetForDrawing(atPoint point: CGPoint) -> CGPoint
    {
        var offset = self.offset
        var size = self.size
        
        if size.width == 0.0 && image != nil
        {
            size.width = image!.size.width
        }
        if size.height == 0.0 && image != nil
        {
            size.height = image!.size.height
        }
        
        let width = size.width
        let height = size.height
        let padding: CGFloat = 8.0
        
        var origin = point
        origin.x -= width / 2
        origin.y -= height
        
        if origin.x + offset.x < 0.0
        {
            offset.x = -origin.x + padding
        } else if let chart = chartView, origin.x + width + offset.x > chart.bounds.size.width {
            offset.x = chart.bounds.size.width - origin.x - width - padding
        }
        
        if origin.y + offset.y < 0
        {
            offset.y = height + padding;
        }
        else if let chart = chartView,
            origin.y + height + offset.y > chart.bounds.size.height
        {
            offset.y = chart.bounds.size.height - origin.y - height - padding
        }
        
        return offset
    }
    
    open override func draw(context: CGContext, point: CGPoint)
    {
        guard let valueLabel = _dateAndValueLabel else { return }
        guard let percentLabel = _percentLabel else { return}
        
        let offset = self.offsetForDrawing(atPoint: point)
        let size = self.size
        
        var rect = CGRect(
            origin: CGPoint(
                x: point.x + offset.x,
                y: point.y + offset.y),
            size: size)
        rect.origin.x -= size.width / 2.0
        rect.origin.y -= size.height
        
        context.saveGState()
        
        context.setFillColor(color.cgColor)
        
        if offset.y > 0
        {
            let roundedRect = CGPath(
                roundedRect: CGRect(x: rect.origin.x, y: rect.origin.y + arrowSize.height, width: rect.width, height: rect.height - arrowSize.height),
                cornerWidth: 5,
                cornerHeight: 5,
                transform: nil
            )

            context.addPath(roundedRect)
            
            context.move(to: CGPoint(
                x: rect.origin.x + (rect.size.width + arrowSize.width) / 2.0,
                y: rect.origin.y  + arrowSize.height
            ))
            
            context.addLine(to: CGPoint(
                x: point.x,
                y: point.y))
            context.addLine(to: CGPoint(
                x: rect.origin.x + (rect.size.width - arrowSize.width) / 2.0,
                y: rect.origin.y + arrowSize.height))
            context.fillPath()
        }
        else
        {
            let roundedRect = CGPath(
                roundedRect: CGRect(x: rect.origin.x, y: rect.origin.y, width: rect.width, height: rect.height - arrowSize.height),
                cornerWidth: 5,
                cornerHeight: 5,
                transform: nil)
            context.addPath(roundedRect)
            
            context.move(to: CGPoint(
                x: rect.origin.x + (rect.size.width + arrowSize.width) / 2.0,
                y: rect.origin.y + rect.size.height - arrowSize.height
            ))
            
            context.addLine(to: CGPoint(
                x: point.x,
                y: point.y))
            context.addLine(to: CGPoint(
                x: rect.origin.x + (rect.size.width - arrowSize.width) / 2.0,
                y: rect.origin.y + rect.size.height - arrowSize.height))
            context.fillPath()
        }
        
        if offset.y > 0 {
            rect.origin.y += self.insets.top + arrowSize.height
        } else {
            rect.origin.y += self.insets.top
        }
        rect.size.height -= self.insets.top + self.insets.bottom
        
        UIGraphicsPushContext(context)
        
        //Percent Label
        let percentRect = CGRect(x: rect.midX - _percentLabelSize.width / 2, y: rect.minY, width: _percentLabelSize.width, height: _percentLabelSize.height)
        percentLabel.draw(in: percentRect, withAttributes: _percentAttributes)
        
        //Percent triangle
        switch triangleType {
        case .Less:
            context.move(to: CGPoint(
                x: percentRect.minX,
                y: percentRect.midY - triangleSize.height / 2
            ))
            context.addLine(to: CGPoint(
                x: percentRect.minX + triangleSize.width / 2,
                y: percentRect.midY + triangleSize.height / 2
            ))
            context.addLine(to: CGPoint(
                x: percentRect.minX + triangleSize.width,
                y: percentRect.midY - triangleSize.height / 2
            ))

            context.setFillColor(red: 1, green: 0, blue: 0, alpha: 1.0)
        case .More:
            context.move(to: CGPoint(
                x: percentRect.minX + triangleSize.width / 2,
                y: percentRect.midY - triangleSize.height/2
            ))
            context.addLine(to: CGPoint(
                x: percentRect.minX,
                y: percentRect.midY + triangleSize.height/2
            ))
            context.addLine(to: CGPoint(
                x: percentRect.minX + triangleSize.width,
                y: percentRect.midY + triangleSize.height/2
            ))

            context.setFillColor(red: 0, green: 1, blue: 0, alpha: 1.0)
        case .Equal:
            fallthrough
        case .Empty:
            break
        }
        context.fillPath()
        
        //Separator
        context.setStrokeColor(self.textColor.cgColor)
        context.move(to: CGPoint(x: rect.minX + 12, y: percentRect.maxY + 8))
        context.addLine(to: CGPoint(x: rect.maxX - 12, y: percentRect.maxY + 8))
        context.strokePath()
        
        //Date and Value
        let dateAndValueRect = CGRect(x: rect.midX - _dateAndValueSize.width / 2, y: rect.maxY - _dateAndValueSize.height, width: _dateAndValueSize.width, height: _dateAndValueSize.height)
        valueLabel.draw(in: dateAndValueRect, withAttributes: _dateAndValueAttributes)
        
        UIGraphicsPopContext()
        
        context.restoreGState()
    }
    
    open override func refreshContent(entry: ChartDataEntry, highlight: Highlight)
    {
        calculateEntriesDifference(entry: entry, highlight: highlight)
        setPercentLabel()
        setValueLabel(entry)
        calculateSize()
    }
    
    private func calculateSize() {
        var size = CGSize()
        size.width = (_dateAndValueSize.width > _percentLabelSize.width ? _dateAndValueSize.width : _percentLabelSize.width) + self.insets.left + self.insets.right
        size.height = _dateAndValueSize.height + self.insets.top + self.insets.bottom + _percentLabelSize.height + _spaceBetweenPercAndValue
        size.width = max(minimumSize.width, size.width)
        size.height = max(minimumSize.height, size.height)
        self.size = size
    }
    
    private func calculateEntriesDifference(entry: ChartDataEntry, highlight: Highlight) {
        guard let chartData = chartView?.data else {
            triangleType = .Empty
            differenceEntryPercent = 0
            return
        }
        let dataSet = chartData.dataSets[highlight.dataSetIndex]
        let highlightIndex = dataSet.entryIndex(entry: entry)
        if let prevEntry = dataSet.entryForIndex(highlightIndex - 1) {
            if entry.y > prevEntry.y {
                triangleType = .More
            } else if entry.y < prevEntry.y {
                triangleType = .Less
            } else {
                triangleType = .Equal
            }
            differenceEntryPercent = CGFloat(entry.y < prevEntry.y ? prevEntry.y / entry.y : entry.y / prevEntry.y)
        } else {
            triangleType = .Empty
            differenceEntryPercent = 0
        }
    }
    
    private func setPercentLabel() {
        _percentLabel = "\((differenceEntryPercent * 100).rounded(.toNearestOrEven) / 100)%"
        _percentLabelSize = _percentLabel?.size(withAttributes: _dateAndValueAttributes) ?? CGSize.zero
        _percentLabelSize = CGSize(width: _percentLabelSize.width + triangleSize.width + 6, height: _percentLabelSize.height)
    }
    
    
    private func setValueLabel(_ entry: ChartDataEntry)
    {
        let xAxis = chartView?.xAxis.valueFormatter?.stringForValue(entry.x, axis: nil) ?? String(entry.x)
        _dateAndValueLabel = "Цена: \(entry.y) \(xAxisMagn)\n" +
        "Дата: \(xAxis)"
        _dateAndValueSize = _dateAndValueLabel?.size(withAttributes: _dateAndValueAttributes) ?? CGSize.zero
    }
}
