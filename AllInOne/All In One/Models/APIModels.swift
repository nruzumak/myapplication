//
//  APIModels.swift
//  All In One
//
//  Created by  MacBook on 28/06/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit
import CoreData

class APIModels {    
    ///Модель курса валюты к выбраной волюте
    class ExchangeRateOfCoin: Codable {
        var name : String
        var exchangeRate: [String: Double]
        
        init(name: String, exchangeRate: [String : Double]) {
            self.name = name
            self.exchangeRate = exchangeRate
        }
        
    }
    
    /// История изменения валюты
    class HistoricalExchangeRate: Codable {
        enum TypeHistoricalRate {
            case Minute
            case Hourly
            case Daily
        }
        let response: String
        let type: Int
        let aggregated: Bool
        let data: [CoinHistory]
        let timeTo : Int
        let timeFrom: Int
        
        enum CodingKeys: String, CodingKey {
            case response = "Response"
            case type = "Type"
            case aggregated = "Aggregated"
            case data = "Data"
            case timeTo = "TimeTo"
            case timeFrom = "TimeFrom"
        }
        
        init(response: String, type: Int, aggregated: Bool, data: [CoinHistory], timeTo: Int, timeFrom: Int) {
            self.response = response
            self.type = type
            self.aggregated = aggregated
            self.data = data
            self.timeTo = timeTo
            self.timeFrom = timeFrom
        }
    }
    
    class CoinInfoResult: Codable {
        let message: String
        let type: Int
        let data: [CoinInfoData]
        let hasWarning: Bool
        
        enum CodingKeys: String, CodingKey {
            case message = "Message"
            case type = "Type"
            case data = "Data"
            case hasWarning = "HasWarning"
        }
        
        init(message: String, type: Int, data: [CoinInfoData], hasWarning: Bool) {
            self.message = message
            self.type = type
            self.data = data
            self.hasWarning = hasWarning
        }
    }
    
    class CoinInfoData: Codable {
        let coinInfo: CoinInfo
        let conversionInfo: ConversionInfo
        var exchangeRate: (currency: String, value: Double)!
        
        enum CodingKeys: String, CodingKey {
            case coinInfo = "CoinInfo"
            case conversionInfo = "ConversionInfo"
        }
        
        init(coinInfo: CoinInfo, conversionInfo: ConversionInfo) {
            self.coinInfo = coinInfo
            self.conversionInfo = conversionInfo
        }
    }

    class CoinInfo: Codable {
        let id, name, fullName, coinInfoInternal: String
        let imageURL, url, algorithm, proofType: String
        var fullImageURL: URL {
            get {
                return URL(string: "https://www.cryptocompare.com\(imageURL)")!
            }
        }
        let netHashesPerSecond: Double
        let blockNumber, blockTime: Int
        let blockReward: Double
        let type: Int
        let documentType: String
        
        enum CodingKeys: String, CodingKey {
            case id = "Id"
            case name = "Name"
            case fullName = "FullName"
            case coinInfoInternal = "Internal"
            case imageURL = "ImageUrl"
            case url = "Url"
            case algorithm = "Algorithm"
            case proofType = "ProofType"
            case netHashesPerSecond = "NetHashesPerSecond"
            case blockNumber = "BlockNumber"
            case blockTime = "BlockTime"
            case blockReward = "BlockReward"
            case type = "Type"
            case documentType = "DocumentType"
        }
        
        init(id: String, name: String, fullName: String, coinInfoInternal: String, imageURL: String, url: String, algorithm: String, proofType: String, netHashesPerSecond: Double, blockNumber: Int, blockTime: Int, blockReward: Double, type: Int, documentType: String) {
            self.id = id
            self.name = name
            self.fullName = fullName
            self.coinInfoInternal = coinInfoInternal
            self.imageURL = imageURL
            self.url = url
            self.algorithm = algorithm
            self.proofType = proofType
            self.netHashesPerSecond = netHashesPerSecond
            self.blockNumber = blockNumber
            self.blockTime = blockTime
            self.blockReward = blockReward
            self.type = type
            self.documentType = documentType
        }
    }
    
    class ConversionInfo: Codable {
        let conversion, conversionSymbol, currencyFrom, currencyTo: String
        let market: String
        let supply: Double
        let totalVolume24H, totalTopTierVolume24H: Double
        let subBase: String
        let subsNeeded, raw: [String]
        
        enum CodingKeys: String, CodingKey {
            case conversion = "Conversion"
            case conversionSymbol = "ConversionSymbol"
            case currencyFrom = "CurrencyFrom"
            case currencyTo = "CurrencyTo"
            case market = "Market"
            case supply = "Supply"
            case totalVolume24H = "TotalVolume24H"
            case totalTopTierVolume24H = "TotalTopTierVolume24H"
            case subBase = "SubBase"
            case subsNeeded = "SubsNeeded"
            case raw = "RAW"
        }
        
        init(conversion: String, conversionSymbol: String, currencyFrom: String, currencyTo: String, market: String, supply: Double, totalVolume24H: Double, totalTopTierVolume24H: Double, subBase: String, subsNeeded: [String], raw: [String]) {
            self.conversion = conversion
            self.conversionSymbol = conversionSymbol
            self.currencyFrom = currencyFrom
            self.currencyTo = currencyTo
            self.market = market
            self.supply = supply
            self.totalVolume24H = totalVolume24H
            self.totalTopTierVolume24H = totalTopTierVolume24H
            self.subBase = subBase
            self.subsNeeded = subsNeeded
            self.raw = raw
        }
    }
    
    
    /// Данные для истории изменения валюты
    class CoinHistory: Codable {
        let time: Int
        let high, low: Double
        var average : Double {
            get {
                return (high + low) / 2
            }
        }
        
        enum CodingKeys: String, CodingKey {
            case time
            case high
            case low
        }
        
        init(time: Int, high: Double, low: Double) {
            self.time = time
            self.high = high
            self.low = low
        }
    }
}

