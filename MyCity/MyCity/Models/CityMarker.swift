//
//  CityProblem.swift
//  MyCity
//
//  Created by  MacBook on 28/05/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import Foundation
import MapKit

class CityMarker : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var tags: [String]
    var subtitle: String?
    let author: String
    var type: CityMarkerType
    var performingUsers: [String]
    
    init(coordinate: CLLocationCoordinate2D, title: String, tags: [String] = [], subtitle: String, author: String, type: CityMarkerType, performingUsers: [String] = []) {
        self.coordinate = coordinate
        self.title = title
        self.tags = tags
        self.subtitle = subtitle
        self.author = author
        self.type = type
        self.performingUsers = performingUsers
        super.init()
    }
}
