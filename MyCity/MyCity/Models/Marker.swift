//
//  CItyMarkerType.swift
//  MyCity
//
//  Created by  MacBook on 17/06/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

class Marker {
    enum MarkerType : String {
        case Ecology
        case HomelessAnimals
        case UnattendedСar
        case Road
        case Other
    }
    
    var type : MarkerType
    var description : String
    var title : String
    var image : UIImage
    
    init(type: MarkerType, description: String, title: String, image: UIImage) {
        self.type = type
        self.description = description
        self.title = title
        self.image = image
    }
}
