//
//  MapViewController.swift
//  MyCity
//
//  Created by  MacBook on 27/05/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit
import MapKit
import PopMenu
import Macaw
import Hero

class MapViewController: UIViewController {
    //MARK: Поля и свойства
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var addButton: UIButton!
    
    let centerLatitude: Double = 55.159552727122076
    let centerLongitude: Double = 61.39467236460692
    let regionRadius: Double = 50000
    private var manuallyChangingMap = false
    var centerLocation: CLLocation {
        get {
            return CLLocation(latitude: centerLatitude, longitude: centerLongitude)
        }
    }
    var restrictedRegion: MKCoordinateRegion  {
        get {
            return MKCoordinateRegion(center: centerLocation.coordinate, span: MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5))
        }
    }
    
    //MARK: Методы
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configSelf()
    }
    
    func configSelf() {
        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.navigationAnimationType = .fade
        let problem1 = CityMarker(coordinate: CLLocationCoordinate2D(latitude: 55.159552727122076, longitude: 61.39467236460692), title: "Мусор во дворе", subtitle: "По адресу 'Ленина23', возле продуктового магазина лежит много пакетов с мусором, скриншоты прилагаются", author: "Никита Воронцов", type: .Ecology)
        mapView.addAnnotation(problem1)
        mapView.register(MKMarkerAnnotationView.self, forAnnotationViewWithReuseIdentifier: "DefaultMarker")
        
        addButton.setImage(#imageLiteral(resourceName: "add-plus"), for: .normal)
    }
    
    //MARK: Эвенты
    @IBAction func addMark(_ sender: Any) {
        let xib = UINib(nibName: "EditOrCreateCityProblem", bundle: nil)
        let viewController = xib.instantiate(withOwner: nil, options: nil)[0] as! UIViewController
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

//MARK: Расширения
extension MapViewController : MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKPolygon {
            let polygonView = MKPolygonRenderer(overlay: overlay)
            polygonView.strokeColor = UIColor.magenta
            polygonView.fillColor = .red
            return polygonView
        } else if overlay is MKCircle {
            let circleView = MKCircleRenderer(overlay: overlay)
            circleView.strokeColor = .gray
            circleView.fillColor = .green
            return circleView
        }
        return MKOverlayRenderer()
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        ///Фиксируем карту на области
        if  ((mapView.region.span.latitudeDelta > restrictedRegion.span.latitudeDelta) ||
            (mapView.region.span.longitudeDelta > restrictedRegion.span.longitudeDelta) ||
            fabs(mapView.region.center.latitude - restrictedRegion.center.latitude) > restrictedRegion.span.latitudeDelta ||
            fabs(mapView.region.center.longitude - restrictedRegion.center.longitude) > restrictedRegion.span.longitudeDelta) {

            mapView.setRegion(restrictedRegion, animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? CityMarker else { return nil }
        let identifier = "DefaultMarker"
        var view: MKMarkerAnnotationView
        let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as! MKMarkerAnnotationView
        dequeuedView.annotation = annotation
        view = dequeuedView
        view.canShowCallout = true
        view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        view.titleVisibility = .hidden
        view.image = #imageLiteral(resourceName: "dead-fish")
        view.glyphImage = #imageLiteral(resourceName: "dead-fish")
        return view
    }
}
