//
//  EditOrCreateCityProblemViewController.swift
//  MyCity
//
//  Created by  MacBook on 30/05/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

class EditOrCreateCityProblemViewController: UIViewController {
    //MARK: Свойства и поля
    @IBOutlet weak var problemTypeCollectionView: UICollectionView!
    
    let problemTypeCollectionViewId = "ProblemTypeCollectionViewCell"
    
    var markerArray : [Marker] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configSelf()
        test()
    }
    
    func test() {
        markerArray.append(Marker(type: .Ecology, description: "Съешь ещё этих мягких французских булок, да выпей же чаю.", title: "Экология", image: #imageLiteral(resourceName: "dead-fish")))
        markerArray
    }
    
    func configSelf() {
        //Настройка problemTypeCollectionView
        problemTypeCollectionView.dataSource = self
        problemTypeCollectionView.delegate = self

        let problemTypeCollectionViewXib = UINib(nibName: problemTypeCollectionViewId, bundle: nil)
        problemTypeCollectionView.register(problemTypeCollectionViewXib, forCellWithReuseIdentifier: problemTypeCollectionViewId)
    }
}

extension EditOrCreateCityProblemViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return problemTypeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: problemTypeCollectionViewId, for: indexPath) as! ProblemTypeCollectionViewCell
        return cell
    }
    
    
}

extension EditOrCreateCityProblemViewController : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Placeholder"
            textView.textColor = UIColor.lightGray
        }
    }
}
