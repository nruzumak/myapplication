//
//  CombinedClock.swift
//  Stopwatch
//
//  Created by  MacBook on 12/04/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit
import Macaw

class CombinedStopwatch : MacawView {
    enum StopwatchStatus {
        case running
        case pause
        case stop
    }
    
    var scene : CombinedStopwatchScene!
    
    var arrowToCircleDistance : Double = 5
    var arrowWidth : Double = 15
    var arrowHeight : Double = 10
    var widthLine : Double = 3
    var size = Size.zero
    
    private(set) var timerInterval : Double = 0.01
    
    private(set) var status : StopwatchStatus = .stop
    private var timer : Timer = Timer()
    var remainingTimeInterval : TimeInterval = TimeInterval(0)
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let newBounds = self.bounds.toMacaw()
        let newSize = newBounds.size()
        if (!(newSize == size)) {
            size = newSize
            updateNode()
        }
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        updateNode()
    }
    
    override init?(node: Node, coder aDecoder: NSCoder) {
        super.init(node: node, coder: aDecoder)
        updateNode()
    }
    
    func updateNode() {
        let scene = CombinedStopwatchScene(combinedStopwatch: self)
        let node = scene.node
        node.place = Transform.move(
            dx: Double(self.center.x),
            dy: Double(self.center.y)
        )
        self.node = node
        self.scene = scene
    }
    func startTimer() {
        switch status {
        case .running:
            break
        case .stop:
            fallthrough
        case .pause:
            timer = Timer.scheduledTimer(withTimeInterval: timerInterval, repeats: true, block: { (timer) in
                self.remainingTimeInterval += self.timerInterval
                self.scene.moveByTimeInterval()
            })
            status = .running
        @unknown default:
            fatalError("Someone gone wrong")
        }
    }
    
    func pauseTimer() {
        if status == .running {
            timer.invalidate()
            status = .pause
        }
    }
    
    func stopTimer() {
        if status != .stop {
            timer.invalidate()
            scene.toDefault()
            remainingTimeInterval = 0
            timer = Timer()
            status = .stop
        }
    }
    
}

class CombinedStopwatchScene {
    let combinedStopwatch : CombinedStopwatch
    let arrow: Node
    let circle: Node
    let numberTime: Node
    let node: Group
    
    init(combinedStopwatch: CombinedStopwatch) {
        self.combinedStopwatch = combinedStopwatch
        let mostLongSide = combinedStopwatch.frame.height < combinedStopwatch.frame.width ? combinedStopwatch.frame.height : combinedStopwatch.frame.width
        let radius = Double(mostLongSide) / 2 - combinedStopwatch.arrowHeight - combinedStopwatch.arrowToCircleDistance - combinedStopwatch.widthLine

        arrow = Polygon(points: [-combinedStopwatch.arrowWidth / 2, -(combinedStopwatch.widthLine + radius + combinedStopwatch.arrowHeight + combinedStopwatch.arrowToCircleDistance) , combinedStopwatch.arrowWidth / 2, -(combinedStopwatch.widthLine + radius + combinedStopwatch.arrowHeight + combinedStopwatch.arrowToCircleDistance), 0, -(combinedStopwatch.widthLine + radius + combinedStopwatch.arrowToCircleDistance)]).fill(with: Color.black)
        
        circle = Shape(form: Circle(cx: 0, cy: 0, r: radius), stroke: Stroke(fill: Color.black, width: combinedStopwatch.widthLine, cap: .butt, join: .miter))
        
        let sideCircleInCircle = (radius * 2) / sqrt(2)
        let fontSize = UIFont.bestFittingFontSize(for: "00:00:00.00", in: CGRect(x: 0, y: 0, width: sideCircleInCircle, height: sideCircleInCircle), fontDescriptor: UIFontDescriptor(name: "PingFangHK-Thin", size: 14))
        
        //На самом деле ниже расположение numberTime сделано таким образом специально
        //Если align сделать center, то numberTime будет дергаться, взавизимости и ширины
        //каждой отдельной цифры
        numberTime = Text(text: "00:00:00.00", font: Font(name: "PingFangHK-Thin", size: Int(fontSize)), align: .min, baseline: .mid, place: .move(dx: 0, dy: 0))
        numberTime.place = numberTime.place.move(dx: -numberTime.bounds!.center().x, dy: 0)
        node = [circle, arrow, numberTime].group()
    }
    func moveByTimeInterval() {
        let angle = combinedStopwatch.timerInterval * Double.pi / 30
        self.arrow.placeVar.value = self.arrow.place.rotate(angle: angle)
        (numberTime as! Text).text = combinedStopwatch.remainingTimeInterval.stringFromTimeInterval()
    }
    
    func toDefault() {
        let alpha = 0 - combinedStopwatch.remainingTimeInterval - Double(Int(combinedStopwatch.remainingTimeInterval / 60) * 60)
        let angle = (alpha * Double.pi / 30)
        let iterationAngle = angle * combinedStopwatch.timerInterval
        self.arrow.placeVar.value = self.arrow.place.rotate(angle: angle)
        (numberTime as! Text).text = "00:00:00.00"
    }
}
