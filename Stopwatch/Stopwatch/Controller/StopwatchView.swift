//
//  StopwatchView.swift
//  Stopwatch
//
//  Created by  MacBook on 12/04/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

class StopwatchViewController: UIViewController {
    @IBOutlet weak var combinedStopwatch: CombinedStopwatch!
    
    @IBAction func stop(_ sender: Any) {
        print("stop=\(combinedStopwatch.remainingTimeInterval)")
        combinedStopwatch.stopTimer()
    }
    @IBAction func start(_ sender: Any) {
        print("start=\(combinedStopwatch.remainingTimeInterval)")
        combinedStopwatch.startTimer()
    }
    @IBAction func pause(_ sender: Any) {
        print("pause=\(combinedStopwatch.remainingTimeInterval)")
        combinedStopwatch.pauseTimer()
    }
    override func viewDidLoad() {
    }
}
