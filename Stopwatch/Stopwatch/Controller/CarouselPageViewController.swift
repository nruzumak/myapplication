//
//  CarouselPageViewController.swift
//  Stopwatch
//
//  Created by  MacBook on 12/04/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

class CarouselPageViewController: UIPageViewController {
    var views : [UIViewController] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
        self.delegate = self
        
        createViews()
        
        if let firstViewController = views.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
        
        let pageControl = UIPageControl.appearance()
        pageControl.pageIndicatorTintColor = UIColor.gray
        pageControl.currentPageIndicatorTintColor = UIColor.black
    }
    
    func createViews() {
        let stopwatchXib = UINib(nibName: "Stopwatch", bundle: nil)
        let stopwatchViewController = stopwatchXib.instantiate(withOwner: nil, options: nil)[0] as! StopwatchViewController
        
        let timerXib = UINib(nibName: "Timer", bundle: nil)
        let timerViewController = timerXib.instantiate(withOwner: nil, options: nil)[0] as! TimerViewController
        
        views.append(contentsOf: [stopwatchViewController, timerViewController])
    }
}

extension CarouselPageViewController : UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    
    func presentationCount(for _: UIPageViewController) -> Int {
        return views.count
    }
    
    func presentationIndex(for _: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first else {
            return 0
        }
        guard let firstViewControllerIndex = views.firstIndex(of: firstViewController) else {
            return 0
        }
        
        return firstViewControllerIndex
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = views.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return views.last
        }
        
        guard views.count > previousIndex else {
            return nil
        }
        
        return views[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = views.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        guard views.count != nextIndex else {
            return views.first
        }
        
        guard views.count > nextIndex else {
            return nil
        }
        
        return views[nextIndex]
    }
}
