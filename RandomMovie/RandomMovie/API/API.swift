//
//  API.swift
//  RandomMovie
//
//  Created by  MacBook on 15/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//
import UIKit

enum Result<Value> {
    case success(Value)
    case failure(Error)
}

struct ConnectionSetting {
    static let sheme = "https"
    static let host : String = "api.themoviedb.org"
    static let apiToken = "ee044de647ad25b4f147aa2142bd2693"
    static var fullUrl : String {
        get {
            let fullUrl = sheme + host
            return fullUrl
        }
    }
}

class API {
    static let sharedAPIService = API()
    
    func sendGet(parameters : [String: Any]?, path: String?, completion: ((Result<Data>) -> Void)?) {
        var urlComponents = URLComponents()
        urlComponents.scheme = ConnectionSetting.sheme
        urlComponents.host = ConnectionSetting.host
        urlComponents.path = "/\(path ?? "")"
        //urlComponents.query = makeParameters(parameters)
        var urlParameters = [URLQueryItem]()
        parameters?.forEach { keyValue in
            urlParameters.append(URLQueryItem(name: keyValue.key, value: String(describing: keyValue.value)))
        }
        
        urlParameters.append(URLQueryItem(name: "api_key", value: ConnectionSetting.apiToken))
        
        urlComponents.queryItems = urlParameters
        
        guard let url = urlComponents.url else {
            let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Could not create URL from components"]) as Error
            completion?(.failure(error))
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 30.0
        config.timeoutIntervalForResource = 30.0
        let session = URLSession(configuration: config)
        
        #if DEBUG
        print("GET on \(url.absoluteString) with parameters=\(urlComponents.query ?? "")")
        #endif
        
        let task = session.dataTask(with: request) { (responseData, response, responseError) in DispatchQueue.main.async {
            if let error = responseError {
                completion?(.failure(error))
            } else if let jsonData = responseData {
                let stringData = String(bytes: jsonData, encoding: .utf8) ?? "Cant decode to string"
                #if DEBUG
                print("jsonData from \(url.absoluteString) = \(stringData)")
                #endif
                completion?(.success(jsonData))
            } else {
                let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data was not retrieved from request"]) as Error
                completion?(.failure(error))
            }
            }
        }
        task.resume()
    }
    
    func getGenres(completion: ((Result<APIModels.IndicatorGenres>) -> Void)?) {
        sendGet(parameters: nil, path: "3/genre/movie/list") { (result) in
            switch result {
            case .success(let data):
                do {
                    let response : APIModels.IndicatorGenres = try self.decodeDataToJson(data: data)
                    completion?(.success(response))
                } catch {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    func getMovies(genres: String, year: Int, minRating: Int, page: Int, completion: ((Result<APIModels.IndicatorMovies>) -> Void)?) {
        var dict : [String : Any] = [:]
        dict["with_genres"] = genres
        dict["primary_release_year"] = year
        dict["page"] = page
        dict["vote_average.gte"] = minRating
        dict["vote_average.lte"] = 10
        dict["include_adult"] = false
        dict["include_video"] = false
        dict["sort_by"] = "popularity.asc"
        
        sendGet(parameters: dict, path: "3/discover/movie") { (result) in
            switch result {
            case .success(let data):
                do {
                    let response : APIModels.IndicatorMovies = try self.decodeDataToJson(data: data)
                    completion?(.success(response))
                } catch {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    func getTopRatedMovie(page: Int = 1, completion: ((Result<APIModels.IndicatorMovies>) -> Void)?) {
        var dict : [String : Any] = [:]
        dict["page"] = page
        
        sendGet(parameters: dict, path: "3/movie/top_rated") { (result) in
            switch result {
            case .success(let data):
                do {
                    let response : APIModels.IndicatorMovies = try self.decodeDataToJson(data: data)
                    completion?(.success(response))
                } catch {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    func getRandomMovie(genres: String, year: Int, minRating: Int, completion: ((Result<APIModels.Movie?>) -> Void)?) {
        var dict : [String : Any] = [:]
        dict["with_genres"] = genres
        dict["year"] = year
        dict["page"] = 1000
        dict["vote_average.gte"] = minRating
        dict["vote_average.lte"] = 10
        dict["include_adult"] = false
        dict["include_video"] = false
        dict["sort_by"] = "popularity.asc"
        
        self.getMovies(genres: genres, year: year, minRating: minRating, page: 1000) { (result) in
            switch result {
            case .success(let fakeMovies):
                    let rndPage = Int.random(in: 1...fakeMovies.totalPages)
                    self.getMovies(genres: genres, year: year, minRating: minRating, page: rndPage) { result in
                        switch result {
                        case .success(let movies):
                            if movies.results.count > 0 {
                                let rndMovie = Int.random(in: 0..<movies.results.count)
                                completion?(.success(movies.results[rndMovie]))
                            } else {
                                completion?(.success(nil))
                            }
                    
                        case .failure(let error):
                            completion?(.failure(error))
                        }
                    }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    func searchMovie(language: String = "en-US", includeAdult: Bool = false, region: String? = nil, query: String? = nil, page: Int = 1, completion: ((Result<APIModels.IndicatorMovies>) -> Void)?) {
        var dict : [String : Any] = [:]
        dict["page"] = page
        dict["include_adult"] = includeAdult
        dict["language"] = language
        dict["query"] = query
        dict["region"] = region
        
        sendGet(parameters: dict, path: "3/search/movie") { (result) in
            switch result {
            case .success(let data):
                do {
                    let response : APIModels.IndicatorMovies = try self.decodeDataToJson(data: data)
                    completion?(.success(response))
                } catch {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    private func decodeDataToJson<T: Codable>(data: Data, itAlphaResult: Bool = true) throws -> T {
        let response = try JSONDecoder().decode(T.self, from: data)
        return response
    }
}
