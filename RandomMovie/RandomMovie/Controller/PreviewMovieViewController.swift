//
//  PreviewLikedMovieViewController.swift
//  RandomMovie
//
//  Created by  MacBook on 25/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit
import Cosmos

class PreviewMovieViewController: UIViewController {
    var canSaving : Bool = false {
        didSet {
            if canSaving {
                let saveBarItem = UIBarButtonItem(image: #imageLiteral(resourceName: "saveIcon"), style: .plain, target: self, action: #selector(saveMovie))
                saveBarItem.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.navigationItem.rightBarButtonItem = saveBarItem
            } else {
                self.navigationItem.rightBarButtonItem = nil
            }
        }
    }
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var ratingCosmos: CosmosView!
    
    @IBOutlet weak var originalTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var voteCountLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    
    var movie : APIModels.Movie! {
        didSet {
            configSelf()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configSelf() {
        descriptionLabel.text = movie.overview
        ratingCosmos.rating = movie.voteAverage / 2
        titleLabel.text = movie.title
        originalTitleLabel.text = movie.originalTitle
        voteCountLabel.text = String(movie.voteCount)
        genresLabel.text = Genre.makeGenresString(movie.genreIDS)
        posterImageView.sd_setImage(with: movie.imageUrl, placeholderImage: #imageLiteral(resourceName: "noImage"))
    }
    @objc func saveMovie() {
        do {
            try LikedMovies.sharedLikedMovies.addItem(APIModels.LikedMovie(movie: movie))
            self.navigationController?.popToRootViewController(animated: true)
        } catch {
            Exceptions.showSimpleAlert(title: "Warning", message: "Movie allready save", controller: self)
        }
        
    }
}
