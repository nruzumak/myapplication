//
//  LikedMoviesViewController.swift
//  RandomMovie
//
//  Created by  MacBook on 25/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit
import SDWebImage

class LikedMoviesViewController: UIViewController {
    
    var likedMovies = LikedMovies.sharedLikedMovies
    
    private let movieCollectionViewCellXib = "LikedMovieCollectionViewCell"
    private let movieCollectionViewCellIdentifier = "LikedMovieCollectionViewCell"
    private let sequePreviewMovieIdentifier = "PreviewMovie"
    
    internal var nothingView : UIView!
    
    @IBOutlet weak var movieCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configSelf()
    }
    
    func configSelf() {
        let xib = UINib(nibName: movieCollectionViewCellXib, bundle: nil)
        movieCollectionView.register(xib, forCellWithReuseIdentifier: movieCollectionViewCellIdentifier)
        NotificationCenter.default.addObserver(self, selector: #selector(dataChanged), name: .didChangeData, object: likedMovies)
    }
    
    @objc func dataChanged() {
        movieCollectionView.reloadData()
    }
}

extension LikedMoviesViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let moviesCount = likedMovies.likedMovies.count
        
        if moviesCount == 0 {
            self.movieCollectionView.showNothingView("NothingView")
        } else {
            self.movieCollectionView.removeNothingView()
        }
        return moviesCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = movieCollectionView.dequeueReusableCell(withReuseIdentifier: movieCollectionViewCellIdentifier, for: indexPath)
        let movieCell = cell as! LikedMovieCollectionViewCell
        
        let likedMovie = likedMovies.likedMovies[indexPath.row]
        
        movieCell.customCellDelegate = self
        movieCell.likedMovie = likedMovie
        
        return movieCell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}

extension LikedMoviesViewController : CustomCellDelegate {
    func presentViewController(_ viewController: UIViewController) {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
