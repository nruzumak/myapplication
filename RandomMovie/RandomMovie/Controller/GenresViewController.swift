//
//  GenresViewController.swift
//  RandomMovie
//
//  Created by  MacBook on 19/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

class GenresViewController: UIViewController {
    @IBOutlet weak var genresTableView: UITableView!
    var genres  = availableGenres
    
    private let sequeDateAndRatingIdentifier = "Date"
    private let genresTableCellXibName = "GenreTableViewCell"
    private let genresTableCellIdentifier = "GenreTableViewCell"
    
    private var selectedGenres : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initSubviews()
    }
    
    func initSubviews() {
        genresTableView.rowHeight = UITableView.automaticDimension
        let xib = UINib(nibName: genresTableCellXibName, bundle: nil)
        genresTableView.register(xib, forCellReuseIdentifier: genresTableCellIdentifier)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? DateAndRatingSelectViewController {
            viewController.genres = selectedGenres
        }
    }
}

extension GenresViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return genres.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = genresTableView.dequeueReusableCell(withIdentifier: genresTableCellIdentifier)
        
        guard let genreCell = cell as? GenreTableViewCell else {
            return UITableViewCell()
        }
        
        let genre = genres[indexPath.row]
        
        genreCell.genreNameLabel.text = genre.name
        genreCell.genreImageView.image = genre.image
        
        return genreCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedGenres = String(genres[indexPath.row].id)
        performSegue(withIdentifier: sequeDateAndRatingIdentifier, sender: nil)
    }
}
