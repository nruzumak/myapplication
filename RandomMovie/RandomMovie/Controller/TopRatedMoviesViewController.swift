//
//  TopRatedMoviesViewController.swift
//  RandomMovie
//
//  Created by  MacBook on 11/04/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

class TopRatedMoviesViewController: UIViewController {
    private var topRatedCurrentPage : Int = 1
    private var searchCurrentPage : Int = 1
    
    private var searchText : String?
    
    private let topRatedMovieCellReuseId = "TopRatedMovieTableViewCell"
    private let topRatedMovieCellXibName = "TopRatedMovieTableViewCell"
    
    private var isSearch : Bool = false {
        didSet {
            self.reloadTableView()
        }
    }
    
    private let loadingCellReuseId = "LoadingTableViewCell"
    private let loadingCellXibName = "LoadingTableViewCell"
    
    var searchMovies : [APIModels.Movie] = []
    var topRatedMovies : [APIModels.Movie] = []
    var displayingMovies : [APIModels.Movie] {
        get {
            if isSearch {
                return searchMovies
            } else {
                return topRatedMovies
            }
        }
    }

    @IBOutlet weak var moviesTableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configSelf()
        getTopRatedMovies(first: true)
    }
    
    private func configSelf() {
        let topRatedNib = UINib(nibName: topRatedMovieCellXibName, bundle: nil)
        moviesTableView.register(topRatedNib, forCellReuseIdentifier: topRatedMovieCellReuseId)
        
        let loadingNib = UINib(nibName: loadingCellXibName, bundle: nil)
        moviesTableView.register(loadingNib, forCellReuseIdentifier: loadingCellReuseId)
        
        refreshControl.addTarget(self, action:
            #selector(TopRatedMoviesViewController.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        refreshControl.tintColor = #colorLiteral(red: 0.09019607843, green: 0.5647058824, blue: 0.2745098039, alpha: 1)
        
        if #available(iOS 10.0, *) {
            moviesTableView.refreshControl = refreshControl
        } else {
            moviesTableView.addSubview(refreshControl)
        }
        
        moviesTableView.estimatedRowHeight = 176
    }
    
    func reloadTableView() {
        DispatchQueue.main.async {
            let loc = self.moviesTableView.contentOffset
            self.moviesTableView.reloadData()
            self.moviesTableView.contentOffset = loc
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        if isSearch {
            API.sharedAPIService.searchMovie(query: searchText, page: 1) { (result) in
                switch result {
                case .success(let indicatorMovies):
                    self.searchCurrentPage = 1
                    self.searchMovies = indicatorMovies.results
                    self.reloadTableView()
                    self.refreshControl.endRefreshing()
                case .failure(_):
                    self.refreshControl.endRefreshing()
                }
            }
        } else {
            API.sharedAPIService.getTopRatedMovie(page: 1) { result in
                switch result {
                case .success(let indicatorMovies):
                    self.topRatedCurrentPage = 1
                    self.topRatedMovies = indicatorMovies.results
                    self.reloadTableView()
                    self.refreshControl.endRefreshing()
                case .failure(_):
                    self.refreshControl.endRefreshing()
                }
            }
        }
    }
    
    func getSearchMovies(query: String, page: Int = 1, first: Bool = false) {
        if first {
            self.view.showLoading()
        }
        API.sharedAPIService.searchMovie(query: query, page: page) { (result) in
            switch result {
            case .success(let indicatorMovies):
                self.searchCurrentPage = indicatorMovies.page
                if first {
                    self.searchMovies = indicatorMovies.results
                    self.reloadTableView()
                    self.view.removeLoading()
                } else {
                    self.searchMovies.append(contentsOf: indicatorMovies.results)
                    let indexPaths = (self.searchMovies.count - indicatorMovies.results.count ..< self.searchMovies.count)
                        .map { IndexPath(row: $0, section: 0) }
                    self.moviesTableView.insertRows(at: indexPaths, with: .none)
                }
            case .failure(let error):
                if first {
                    self.view.removeLoading()
                }
            }
        }
    }
    
    func getTopRatedMovies(_ page: Int = 1, first: Bool = false) {
        if first {
            self.view.showLoading()
        }
        API.sharedAPIService.getTopRatedMovie(page: page) { result in
            switch result {
            case .success(let indicatorMovies):
                self.topRatedCurrentPage = indicatorMovies.page
            
                if first {
                    self.topRatedMovies = indicatorMovies.results
                    self.reloadTableView()
                    self.view.removeLoading()
                } else {
                    self.topRatedMovies.append(contentsOf: indicatorMovies.results)
                    let indexPaths = (self.topRatedMovies.count - indicatorMovies.results.count ..< self.topRatedMovies.count)
                        .map { IndexPath(row: $0, section: 0) }
                    self.moviesTableView.insertRows(at: indexPaths, with: .none)
                }
            case .failure(let error):
                if first {
                    self.view.removeLoading()
                }
            }
        }
    }
    
    @IBAction func switchSearch(_ sender: Any) {
        if self.navigationItem.titleView as? UISearchBar != nil {
            UIView.animate(withDuration: 0.25, animations: {
                self.navigationItem.titleView!.alpha = 0
            }, completion: { finished in
                self.navigationItem.titleView = nil
                self.searchText = nil
                self.isSearch = false
            })
        } else {
            let searchBar = UISearchBar()
            searchBar.tintColor = #colorLiteral(red: 0.09019607843, green: 0.5647058824, blue: 0.2745098039, alpha: 1)
            searchBar.delegate = self
            searchBar.alpha = 0
            searchBar.showsCancelButton = false
            searchBar.placeholder = "Title"
            self.navigationItem.titleView = searchBar
            UIView.animate(withDuration: 0.25, animations: {
                searchBar.alpha = 1
            }, completion: { finished in
                searchBar.becomeFirstResponder()
            })
        }
    }
}

extension TopRatedMoviesViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = displayingMovies.count
        if count == 0 {
            moviesTableView.showNothingView("NothingView")
        } else {
            moviesTableView.removeNothingView()
        }
        return count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = displayingMovies[indexPath.row]
        let viewController = UINib(nibName: "PreviewMovie", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PreviewMovieViewController
        viewController.movie = movie
        viewController.canSaving = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == displayingMovies.count - 4 {
            if isSearch {
                if let searchText = searchText {
                    getSearchMovies(query: searchText, page: self.searchCurrentPage, first: false)
                }
            } else {
                getTopRatedMovies(topRatedCurrentPage + 1, first: false)
            }
            
        }
        let movieCell = moviesTableView.dequeueReusableCell(withIdentifier: topRatedMovieCellReuseId, for: indexPath) as! TopRatedTableViewCell
        movieCell.movie = self.displayingMovies[indexPath.row]
        return movieCell
    }
}

extension TopRatedMoviesViewController : UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let searchText = searchBar.text {
            self.searchText = searchText
            getSearchMovies(query: searchText, first: true)
            self.isSearch = true
            searchBar.endEditing(true)
        }
    }
}
