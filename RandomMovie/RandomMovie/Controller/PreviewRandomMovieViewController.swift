//
//  PreviewRandomMovieViewController.swift
//  RandomMovie
//
//  Created by  MacBook on 21/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit
import Cosmos

class PreviewRandomMovieViewController: UIViewController {
    var genres : String!
    var year : Int!
    var rating : Int!
    
    var movie : APIModels.Movie! {
        didSet {
            if movie == nil {
                self.view.showNothingView("NothingView")
            } else {
                self.view.removeNothingView()
                setMovieInfo()
            }
        }
    }
    
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var ratingCosmos: CosmosView!
    
    @IBOutlet weak var originalTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var voteCountLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getRandomMovie()
    }
    
    func getRandomMovie() {
        self.view.showLoading()
        API.sharedAPIService.getRandomMovie(genres: genres, year: year, minRating: rating) { result in
            switch result {
            case .success(let movie):
                self.movie = movie
            case .failure(let error):
                Exceptions.showSimpleAlert(title: "Warning", message: error.localizedDescription, controller: self)
            }
            self.view.removeLoading()
        }
    }
    func setMovieInfo() {
        posterImageView.sd_setImage(with: movie.imageUrl, placeholderImage: #imageLiteral(resourceName: "noImage"))
        descriptionLabel.text = movie.overview
        ratingCosmos.rating = movie.voteAverage / 2
        titleLabel.text = movie.title
        originalTitleLabel.text = movie.originalTitle
        voteCountLabel.text = String(movie.voteCount)
        genresLabel.text = Genre.makeGenresString(movie.genreIDS)
    }
    
    @IBAction func addMovie(_ sender: Any) {
        if movie != nil {
            let randomMovie = APIModels.RandomMovie(movie: movie)
            do {
                try RandomMovies.sharedRandomMovies.addItem(randomMovie)
                self.navigationController?.popToRootViewController(animated: true)
            } catch {
                Exceptions.showSimpleAlert(title: "Warning", message: "\(randomMovie.title) is allready in saved list", controller: self)
            }
        } else {
            Exceptions.showSimpleAlert(title: "Warning", message: "I have nothing to save :(", controller: self)
        }
    }
    
    @IBAction func refreshMovie(_ sender: Any) {
        getRandomMovie()
    }
}
