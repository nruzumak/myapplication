//
//  DateSelectViewController.swift
//  RandomMovie
//
//  Created by  MacBook on 19/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

class DateAndRatingSelectViewController: UIViewController {
    @IBOutlet weak var datePicker: UIPickerView!
    @IBOutlet weak var ratingPicker: UIPickerView!
    
    let sequePreviewRandomMovieIdentifier = "previewRandomMovie"
    
    var genres : String!
    private var selectedYear : Int!
    private var selectedRating : Int!
    
    private let ratingNumbers = Array(0...10)
    private let years = Array(1885...(Calendar.current.component(.year, from: Date())))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configSelf()
    }
    
    func configSelf() {
        let yearMiddleRow = years.count / 2
        let ratingMiddleRow = ratingNumbers.count / 2
        datePicker.selectRow(yearMiddleRow, inComponent: 0, animated: false)
        ratingPicker.selectRow(ratingMiddleRow, inComponent: 0, animated: false)
        
        selectedYear = years[yearMiddleRow]
        selectedRating = ratingNumbers[ratingMiddleRow]
    }
    @IBAction func doneClicked(_ sender: Any) {
        performSegue(withIdentifier: sequePreviewRandomMovieIdentifier, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? PreviewRandomMovieViewController {
            viewController.genres = genres
            viewController.rating = selectedRating
            viewController.year = selectedYear
        }
    }
}

extension DateAndRatingSelectViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var title : String = ""
        if pickerView == datePicker {
            title = String(years[row])
        } else if pickerView == ratingPicker {
            title = String(ratingNumbers[row])
        }
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == datePicker {
            selectedYear = years[row]
        } else if pickerView == ratingPicker {
            selectedRating = ratingNumbers[row]
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var count : Int = 0
        if pickerView == datePicker {
            count = years.count
        } else if pickerView == ratingPicker {
            count = ratingNumbers.count
        }
        return count
    }
}
