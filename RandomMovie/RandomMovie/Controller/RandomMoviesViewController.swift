//
//  MainViewController.swift
//  RandomMovie
//
//  Created by  MacBook on 18/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

class RandomMoviesViewController: UIViewController {
    @IBOutlet var addButton: UIButton!
    @IBOutlet weak var movieCollectionView: UICollectionView!
    
    private let movieCollectionViewCellXib = "RandomMovieCollectionViewCell"
    private let movieCollectionViewCellIdentifier = "RandomMovieCollectionViewCell"
    
    private let sequePreviewMovieIdentifier = "PreviewMovie"
    private let sequeGenresIdentifier = "Genres"
    
    var randomMovies = RandomMovies.sharedRandomMovies
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configSelf()   
    }
    
    func configSelf() {
        let xib = UINib(nibName: movieCollectionViewCellXib, bundle: nil)
        movieCollectionView.register(xib, forCellWithReuseIdentifier: movieCollectionViewCellIdentifier)
        addButton.setImage(#imageLiteral(resourceName: "addIcon"), for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(dataChanged), name: .didChangeData, object: randomMovies)
    }
    
    @objc func dataChanged() {
        movieCollectionView.reloadData()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    @IBAction func addButtonClick(_ sender: UIButton) {
        performSegue(withIdentifier: sequeGenresIdentifier, sender: nil)
    }
}

extension RandomMoviesViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collect
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let moviesCount = randomMovies.randomMovies.count
        
        if moviesCount < 1 {
            self.movieCollectionView.showNothingView("NothingView")
        } else {
            self.movieCollectionView.removeNothingView()
        }
        return moviesCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = movieCollectionView.dequeueReusableCell(withReuseIdentifier: movieCollectionViewCellIdentifier, for: indexPath)
        let movieCell = cell as! RandomMovieCollectionViewCell
        
        let randomMovie = randomMovies.randomMovies[indexPath.row]

        movieCell.customCellDelegate = self
        movieCell.randomMovie = randomMovie
        
        return movieCell
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
}

extension RandomMoviesViewController : CustomCellDelegate {
    func presentViewController(_ viewController: UIViewController) {
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
