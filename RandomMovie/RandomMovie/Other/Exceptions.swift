//
//  Exceptions.swift
//  RandomMovie
//
//  Created by  MacBook on 25/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import Foundation
import UIKit

class Exceptions {
    /*
     TODO: Сделать функцию в которую вкидываешь ошибку, она смотрить код ошибки, если это ошибка токена
     то модально выводи страницу логина и жди пока он авторизуется (или вообще все до root страницы скидывай и
     страницу логина покажи, скорее всего это надо где парсим делать)
     */
    
    static func showSimpleAlert(title: String?, message: String?,
                                controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController?.presentedViewController) {
        guard let viewController = controller else {
            return
        }
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "ОК", style: .default, handler: nil)
        alertController.addAction(alertAction)
        viewController.present(alertController, animated: true, completion: nil)
    }
}
