//
//  GlobalExtensions.swift
//  RandomMovie
//
//  Created by  MacBook on 22/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView

extension String{
    var encodeUrl : String
    {
        return self.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!
    }
    var decodeUrl : String
    {
        return self.removingPercentEncoding!
    }
}

extension UIView {
    private struct Holder {
        static var loadingViews = [String: UIView]()
        static var nothingViews = [String: UIView]()
    }
}

extension UIView {
    var nothingView: UIView! {
        get {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: UIView.self))
            return Holder.nothingViews[tmpAddress] ?? nil
        }
        set(newValue) {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: UIView.self))
            if Holder.nothingViews[tmpAddress] != nil {
                Holder.nothingViews[tmpAddress]!.removeFromSuperview()
                Holder.nothingViews[tmpAddress] = nil
            }
            Holder.nothingViews[tmpAddress] = newValue
        }
    }
    func showNothingView(_ nothingViewIdentifier: String, at belowSubview : UIView? = nil) {
        
        let xib = UINib(nibName: nothingViewIdentifier, bundle: nil)
        self.nothingView = xib.instantiate(withOwner: nil, options: nil)[0] as! UIView
        nothingView.translatesAutoresizingMaskIntoConstraints = false
        
        if let belowView = belowSubview {
            self.insertSubview(nothingView, belowSubview: belowView)
        } else {
            self.addSubview(nothingView)
        }
        
        let xConstraint = NSLayoutConstraint(item: nothingView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: nothingView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: nothingView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: nothingView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0)
        
        self.addConstraint(xConstraint)
        self.addConstraint(yConstraint)
        self.addConstraint(widthConstraint)
        self.addConstraint(heightConstraint)
    }
    
    func removeNothingView() {
        self.nothingView?.removeFromSuperview()
        self.nothingView = nil
    }
}

extension UIView {
    var loadingView: UIView! {
        get {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: UIView.self))
            return Holder.loadingViews[tmpAddress] ?? nil
        }
        set(newValue) {
            let tmpAddress = String(format: "%p", unsafeBitCast(self, to: UIView.self))
            Holder.loadingViews[tmpAddress] = newValue
        }
    }
    func showLoading() {
        loadingView?.removeFromSuperview()
        loadingView = nil
        loadingView = NVActivityIndicatorView(frame: self.frame, type: NVActivityIndicatorType.ballRotateChase, color: #colorLiteral(red: 0.09019607843, green: 0.5647058824, blue: 0.2745098039, alpha: 1), padding: self.frame.width / 2.3)
        (loadingView as! NVActivityIndicatorView).startAnimating()
        
        loadingView.backgroundColor = .white
        loadingView.translatesAutoresizingMaskIntoConstraints = false

        let xConstraint = NSLayoutConstraint(item: loadingView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: loadingView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: loadingView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: loadingView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0)
        
        self.addSubview(loadingView)
        
        self.addConstraint(xConstraint)
        self.addConstraint(yConstraint)
        self.addConstraint(widthConstraint)
        self.addConstraint(heightConstraint)

        UIView.animate(withDuration: 0.5, animations: {
            self.loadingView?.alpha = 1
        }, completion: { (finished: Bool) in
            self.loadingView?.alpha = 1
        })
    }
    
    
    func removeLoading() {
        UIView.animate(withDuration: 0.5, animations: {
            self.loadingView?.alpha = 0
        }, completion: { (finished: Bool) in
            self.loadingView?.alpha = 0
            DispatchQueue.main.async {
                self.loadingView?.removeFromSuperview()
                self.loadingView = nil
            }
        })
    }
}

extension Notification.Name {
    static let didChangeData = Notification.Name("didChangeData")
}

extension AppDelegate {
    class var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    class func getTopMostViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopMostViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopMostViewController(base: selected)
            }
        }
        if let split = base as? UISplitViewController {
            return getTopMostViewController(base: split.viewControllers[1])
        }
        if let presented = base?.presentedViewController {
            return getTopMostViewController(base: presented)
        }
        return base
    }
}
