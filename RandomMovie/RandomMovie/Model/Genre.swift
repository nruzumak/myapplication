//
//  Genre.swift
//  RandomMovie
//
//  Created by  MacBook on 11/04/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

class Genre {
    let id: Int
    let name: String
    let image: UIImage
    
    init(id: Int, name: String, image: UIImage) {
        self.id = id
        self.name = name
        self.image = image
    }
    
    static func makeGenresString(_ idGenres: [Int]) -> String {
        var genres : [String] = []
        
        idGenres.forEach() { id in
            availableGenres.forEach() { genre in
                id == genre.id ? genres.append(genre.name) : nil
            }
        }
        if genres.count != 0 {
            return genres.joined(separator: ", ")
        } else {
            return "other"
        }
    }
    
    static func makeGenres(_ idGenres: [Int]) -> [Genre] {
        var genres : [Genre] = []
        
        idGenres.forEach() { id in
            availableGenres.forEach() { genre in
                id == genre.id ? genres.append(genre) : nil
            }
        }
        return genres
    }
}

let availableGenres : [Genre] = [
    Genre(id: 28, name: "Action", image: #imageLiteral(resourceName: "actionMovieIcon")),
    Genre(id: 12, name: "Adventure", image: #imageLiteral(resourceName: "adventureMovieIcon")),
    Genre(id: 16, name: "Animation", image: #imageLiteral(resourceName: "animationMovieIcon")),
    Genre(id: 35, name: "Comedy", image: #imageLiteral(resourceName: "ComedyMovieIcon")),
    Genre(id: 80, name: "Crime", image: #imageLiteral(resourceName: "crimeMovieIcon")),
    Genre(id: 99, name: "Documentary", image: #imageLiteral(resourceName: "documentaryMovieIcon")),
    Genre(id: 18, name: "Drama", image: #imageLiteral(resourceName: "dramaMovieIcon")),
    Genre(id: 10751, name: "Family", image: #imageLiteral(resourceName: "familyMovieIcon")),
    Genre(id: 14, name: "Fantasy", image: #imageLiteral(resourceName: "fantasyMovieIcon")),
    Genre(id: 36, name: "History", image: #imageLiteral(resourceName: "historyMovieIcon")),
    Genre(id: 27, name: "Horror", image: #imageLiteral(resourceName: "horrorMovieIcon")),
    Genre(id: 10402, name: "Music", image: #imageLiteral(resourceName: "musicMovieIcon")),
    Genre(id: 9648, name: "Mystery", image: #imageLiteral(resourceName: "mysteryMovieIcon")),
    Genre(id: 10749, name: "Romance", image: #imageLiteral(resourceName: "romanceMovieIcon")),
    Genre(id: 878, name: "Science Fiction", image: #imageLiteral(resourceName: "scienceFictionMovieIcon")),
    Genre(id: 10770, name: "TV Movie", image: #imageLiteral(resourceName: "TVMovieIcon")),
    Genre(id: 53, name: "Thriller", image: #imageLiteral(resourceName: "thrillerMovieIcon")),
    Genre(id: 10752, name: "War", image: #imageLiteral(resourceName: "warMovieIcon")),
    Genre(id: 37, name: "Western", image: #imageLiteral(resourceName: "westernMovieIcon"))
]
