//
//  APIModels.swift
//  RandomMovie
//
//  Created by  MacBook on 18/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

class APIModels {
    class IndicatorGenres: Codable {
        let genres: [Genre]
        
        init(genres: [Genre]) {
            self.genres = genres
        }
    }
    
    class Genre: Codable {
        let id: Int
        let name: String
        
        init(id: Int, name: String) {
            self.id = id
            self.name = name
        }
    }
    
    class IndicatorMovies: Codable {
        let page, totalResults, totalPages: Int
        let results: [Movie]
        
        enum CodingKeys: String, CodingKey {
            case page
            case totalResults = "total_results"
            case totalPages = "total_pages"
            case results
        }
        
        init(page: Int, totalResults: Int, totalPages: Int, results: [Movie]) {
            self.page = page
            self.totalResults = totalResults
            self.totalPages = totalPages
            self.results = results
        }
    }
    
    class Movie: Codable, Equatable {
        enum SizeImage: String {
            case Original = "Original"
            case w154 = "w154"
        }
        
        static func == (lhs: Movie, rhs: Movie) -> Bool {
            return lhs.id == rhs.id
        }
        let voteCount, id: Int
        let video: Bool
        let voteAverage: Double
        let title: String
        let popularity: Double
        let posterPath: String?
        let originalLanguage, originalTitle: String
        let genreIDS: [Int]
        let backdropPath: String?
        let adult: Bool
        let overview, releaseDate: String
        var imageUrl : URL? {
            get {
                if let posterPath = posterPath {
                    let fullPath = "https://image.tmdb.org/t/p/original" + posterPath.encodeUrl
                    let url = URL(string: fullPath)
                    return url
                } else {
                    return nil
                }
            }
        }
        
        func imageUrl(size: SizeImage) -> URL?{
            if let posterPath = posterPath {
                let fullPath = "https://image.tmdb.org/t/p/\(size.rawValue)" + posterPath.encodeUrl
                let url = URL(string: fullPath)
                return url
            } else {
                return nil
            }
        }
        
        private enum CodingKeys: String, CodingKey {
            case voteCount = "vote_count"
            case id, video
            case voteAverage = "vote_average"
            case title, popularity
            case posterPath = "poster_path"
            case originalLanguage = "original_language"
            case originalTitle = "original_title"
            case genreIDS = "genre_ids"
            case backdropPath = "backdrop_path"
            case adult, overview
            case releaseDate = "release_date"
        }
        
        init(voteCount: Int, id: Int, video: Bool, voteAverage: Double, title: String, popularity: Double, posterPath: String?, originalLanguage: String, originalTitle: String, genreIDS: [Int], backdropPath: String?, adult: Bool, overview: String, releaseDate: String) {
            self.voteCount = voteCount
            self.id = id
            self.video = video
            self.voteAverage = voteAverage
            self.title = title
            self.popularity = popularity
            self.posterPath = posterPath
            self.originalLanguage = originalLanguage
            self.originalTitle = originalTitle
            self.genreIDS = genreIDS
            self.backdropPath = backdropPath
            self.adult = adult
            self.overview = overview
            self.releaseDate = releaseDate
        }
        
        func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(voteCount, forKey: .voteCount)
            try container.encode(id, forKey: .id)
            try container.encode(video, forKey: .video)
            try container.encode(voteAverage, forKey: .voteAverage)
            try container.encode(title, forKey: .title)
            try container.encode(popularity, forKey: .popularity)
            try container.encode(posterPath, forKey: .posterPath)
            try container.encode(originalLanguage, forKey: .originalLanguage)
            try container.encode(originalTitle, forKey: .originalTitle)
            try container.encode(releaseDate, forKey: .releaseDate)
            try container.encode(genreIDS, forKey: .genreIDS)
            try container.encode(backdropPath, forKey: .backdropPath)
            try container.encode(adult, forKey: .adult)
            try container.encode(overview, forKey: .overview)
        }
        
        required init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.voteCount = try container.decode(Int.self, forKey: .voteCount)
            self.id = try container.decode(Int.self, forKey: .id)
            self.video = try container.decode(Bool.self, forKey: .video)
            self.voteAverage = try container.decode(Double.self, forKey: .voteAverage)
            self.title = try container.decode(String.self, forKey: .title)
            self.popularity = try container.decode(Double.self, forKey: .popularity)
            self.posterPath = try container.decode(String?.self, forKey: .posterPath)
            self.originalLanguage = try container.decode(String.self, forKey: .originalLanguage)
            self.originalTitle = try container.decode(String.self, forKey: .originalTitle)
            self.genreIDS = try container.decode([Int].self, forKey: .genreIDS)
            self.backdropPath = try container.decode(String?.self, forKey: .backdropPath)
            self.adult = try container.decode(Bool.self, forKey: .adult)
            self.overview = try container.decode(String.self, forKey: .overview)
            self.releaseDate = try container.decode(String.self, forKey: .releaseDate)
        }
        
        deinit {
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    class RandomMovie: Movie {
        var isLiked : Bool {
            get {
                return LikedMovies.sharedLikedMovies.likedMovies.first { (likedMovie) -> Bool in
                    return likedMovie == self
                } != nil
            }
        }
        required init(movie: Movie) {
            super.init(voteCount: movie.voteCount, id: movie.id, video: movie.video, voteAverage: movie.voteAverage, title: movie.title, popularity: movie.popularity, posterPath: movie.posterPath, originalLanguage: movie.originalLanguage, originalTitle: movie.originalTitle, genreIDS: movie.genreIDS, backdropPath: movie.backdropPath, adult: movie.adult, overview: movie.overview, releaseDate: movie.releaseDate)
        }
        
        required init(from decoder: Decoder) throws {
            try super.init(from: decoder)
        }
    }
    
    class LikedMovie: Movie {
        var isLooked : Bool {
            didSet{
                NotificationCenter.default.post(name: .didChangeData, object: self)
            }
        }
        
        private enum CodingKeys: String, CodingKey {
            case isLooked = "IsLooked"
        }
        
        required init(movie: Movie, isLooked: Bool = false) {
            self.isLooked = isLooked
            super.init(voteCount: movie.voteCount, id: movie.id, video: movie.video, voteAverage: movie.voteAverage, title: movie.title, popularity: movie.popularity, posterPath: movie.posterPath, originalLanguage: movie.originalLanguage, originalTitle: movie.originalTitle, genreIDS: movie.genreIDS, backdropPath: movie.backdropPath, adult: movie.adult, overview: movie.overview, releaseDate: movie.releaseDate)
        }
        
        override func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(isLooked, forKey: .isLooked)
            try super.encode(to: encoder)
        }
        
        required init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            self.isLooked = try container.decode(Bool.self, forKey: .isLooked)
            try super.init(from: decoder)
        }
    }
}
