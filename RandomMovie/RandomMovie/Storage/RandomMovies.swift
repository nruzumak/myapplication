//
//  SavedRandomMovie.swift
//  RandomMovie
//
//  Created by  MacBook on 22/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import Foundation
import UIKit

class RandomMovies {
    static let sharedRandomMovies = RandomMovies()
    
    private let storedKey = "randomMovies"
    private let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    
    private(set) var randomMovies : [APIModels.RandomMovie]
    
    init() {
        let ArchiveURL = DocumentsDirectory.appendingPathComponent(storedKey)
        if let randomMoviesData = try? Data(contentsOf: ArchiveURL){
            randomMovies = try! PropertyListDecoder().decode([APIModels.RandomMovie].self, from: randomMoviesData)
        } else {
            randomMovies = []
        }
        randomMovies.forEach { (randomMovie) in
            NotificationCenter.default.addObserver(self, selector: #selector(saveItems), name: .didChangeData, object: randomMovie)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(saveItems), name: .didChangeData, object: LikedMovies.sharedLikedMovies)
    }
    
    func addItem(_ movie: APIModels.RandomMovie) throws {
        
        if let _movie = randomMovies.first(where: { (randomMovie) -> Bool in
            return randomMovie == movie
        }){
            throw NSError(domain: "LikedMovie", code: 0, userInfo: ["Movie" : _movie])
        } else {
            randomMovies.append(movie)
            NotificationCenter.default.addObserver(self, selector: #selector(saveItems), name: .didChangeData, object: movie)
            saveItems()
        }
    }
    
    func removeItem(_ movie: APIModels.RandomMovie) {
        randomMovies.removeAll { (randomMovie) -> Bool in
            return randomMovie == movie
        }
        saveItems()
    }
    
    func clearAll() {
        randomMovies.removeAll()
        saveItems()
    }
    
    @objc private func saveItems() {        
        let ArchiveURL = DocumentsDirectory.appendingPathComponent(storedKey)
        do {
            let data = try PropertyListEncoder().encode(randomMovies)
            try data.write(to: ArchiveURL, options: .noFileProtection)
            NotificationCenter.default.post(name: .didChangeData, object: self)
            #if DEBUG
            print("Saving RandomMovies is success")
            #endif
        } catch {
            #if DEBUG
            print("Saving RandomMovies is false")
            #endif
        }
    }
}
