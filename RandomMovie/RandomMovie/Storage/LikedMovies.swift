//
//  LikedMovies.swift
//  RandomMovie
//
//  Created by  MacBook on 23/05/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import Foundation

class LikedMovies {
    static let sharedLikedMovies = LikedMovies()
    
    private let storedKey = "likedMovies"
    private let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    
    private(set) var likedMovies : [APIModels.LikedMovie]
    
    init() {
        let ArchiveURL = DocumentsDirectory.appendingPathComponent(storedKey)
        if let likedMoviesData = try? Data(contentsOf: ArchiveURL){
            likedMovies = try! PropertyListDecoder().decode([APIModels.LikedMovie].self, from: likedMoviesData)
        } else {
            likedMovies = []
        }
        
        likedMovies.forEach { (likedMovie) in
            NotificationCenter.default.addObserver(self, selector: #selector(saveItems), name: .didChangeData, object: likedMovie)
        }
    }
    
    func addItem(_ movie: APIModels.LikedMovie) throws {
        
        if let _movie = likedMovies.first(where: { (likedMovie) -> Bool in
            return likedMovie == movie
        }){
            throw NSError(domain: "LikedMovie", code: 0, userInfo: ["Movie" : _movie])
        } else {
            likedMovies.append(movie)
            NotificationCenter.default.addObserver(self, selector: #selector(saveItems), name: .didChangeData, object: movie)
            saveItems()
        }
    }
    
    func removeItem(_ movie: APIModels.Movie) {
        likedMovies.removeAll { (likedMovie) -> Bool in
            return likedMovie == movie
        }
        saveItems()
    }
    
    func clearAll() {
        likedMovies.removeAll()
        saveItems()
    }
    
    @objc private func saveItems() {
        let ArchiveURL = DocumentsDirectory.appendingPathComponent(storedKey)
        do {
            let data = try PropertyListEncoder().encode(likedMovies)
            try data.write(to: ArchiveURL, options: .noFileProtection)
            NotificationCenter.default.post(name: .didChangeData, object: self)
            #if DEBUG
            print("Saving LikedMovies is success")
            #endif
        } catch {
            #if DEBUG
            print("Saving LikedMovies is false")
            #endif
        }
    }
}
