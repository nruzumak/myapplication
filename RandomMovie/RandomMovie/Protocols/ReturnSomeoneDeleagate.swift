//
//  ReturnSomeoneDeleagate.swift
//  RandomMovie
//
//  Created by  MacBook on 23/05/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import Foundation

protocol ReturnSomeoneDelegate {
    func returnSomeone(_ someone: Any)
}
