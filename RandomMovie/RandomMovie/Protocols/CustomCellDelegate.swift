//
//  CustomCellDelegate.swift
//  RandomMovie
//
//  Created by  MacBook on 23/05/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import Foundation
import UIKit

@objc protocol CustomCellDelegate {
    @objc optional func presentViewController(_ viewController : UIViewController)
    @objc optional func deleteCell(_ cell: UITableViewCell)
    @objc optional func deleteCell(_ cell: UICollectionViewCell)
}
