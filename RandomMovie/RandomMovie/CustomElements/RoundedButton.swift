//
//  RoundedButton.swift
//  RandomMovie
//
//  Created by  MacBook on 18/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//
import UIKit

@IBDesignable
class RoundedButton: UIButton {
        
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var shadow: Float = 1{
        didSet{
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize(width: -2, height: 2)
            self.layer.shadowOpacity = shadow
            self.layer.shadowRadius = 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
