//
//  ScaleImageView.swift
//  RandomMovie
//
//  Created by  MacBook on 19/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

class ScaleImageView : UIImageView {
    
    var scaledImage : UIImage? {
        get {
            guard let image = self.image else {
                return nil
            }
            return resizeImage(image: image)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func resizeImage(image: UIImage) -> UIImage {
        let rect = contentClippingRect
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 1.0)
        self.draw(rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    var contentClippingRect: CGRect {
        guard let image = image else { return bounds }
        guard contentMode == .scaleAspectFit else { return bounds }
        guard image.size.width > 0 && image.size.height > 0 else { return bounds }
        
        let scale: CGFloat
        if image.size.width > image.size.height {
            scale = bounds.width / image.size.width
        } else {
            scale = bounds.height / image.size.height
        }
        
        let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
        let x = (bounds.width - size.width) / 2.0
        let y = (bounds.height - size.height) / 2.0
        
        return CGRect(x: x, y: y, width: size.width, height: size.height)
    }
}
