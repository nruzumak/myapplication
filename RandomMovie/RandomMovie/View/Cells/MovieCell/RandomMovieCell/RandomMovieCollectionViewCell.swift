//
//  MovieViewCell.swift
//  RandomMovie
//
//  Created by  MacBook on 18/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit

class RandomMovieCollectionViewCell : UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var likeBarItem: UIBarButtonItem!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var actionsToolbar: UIToolbar!
    
    var customCellDelegate: CustomCellDelegate!
    
    var randomMovie: APIModels.RandomMovie! {
        didSet {
            if randomMovie != nil {
                configSelf()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configSelf() {
        if randomMovie.isLiked {
            likeBarItem.image = #imageLiteral(resourceName: "flikeFillIcon")
        } else {
            likeBarItem.image = #imageLiteral(resourceName: "likeIcon")
        }
        posterImageView.sd_setImage(with: randomMovie.imageUrl, placeholderImage: #imageLiteral(resourceName: "noImage"))
        titleLabel.text = randomMovie.title
    }
    
    @IBAction func deleteMovie(_ sender: UIBarButtonItem) {
        RandomMovies.sharedRandomMovies.removeItem(randomMovie)
    }
    @IBAction func likeMovie(_ sender: UIBarButtonItem) {
        if randomMovie.isLiked {
            LikedMovies.sharedLikedMovies.removeItem(randomMovie)
            likeBarItem.image = #imageLiteral(resourceName: "likeIcon")
        } else {
            let likedMovie = APIModels.LikedMovie(movie: randomMovie)
            do {
                try LikedMovies.sharedLikedMovies.addItem(likedMovie)
            } catch {
                Exceptions.showSimpleAlert(title: "Warning", message: "\(randomMovie.title) allready in liked list")
            }
            likeBarItem.image = #imageLiteral(resourceName: "flikeFillIcon")
        }
    }
    @IBAction func moreInfoMovie(_ sender: UIBarButtonItem) {        
        let viewController = UINib(nibName: "PreviewMovie", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PreviewMovieViewController
        viewController.movie = randomMovie
        customCellDelegate?.presentViewController?(viewController)
    }
}
