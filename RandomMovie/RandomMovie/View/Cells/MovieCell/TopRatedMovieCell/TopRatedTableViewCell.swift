//
//  TopRatedTableViewCell.swift
//  RandomMovie
//
//  Created by  MacBook on 11/04/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit
import Cosmos

class TopRatedTableViewCell: UITableViewCell {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var originalTitleLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var votesLabel: UILabel!
    @IBOutlet weak var cosmosView: CosmosView!
    
    var customCellDelegate : CustomCellDelegate!
    
    var movie : APIModels.Movie! {
        didSet {
            if movie != nil {
                configSelf()
            }
        }
    }
    
    func configSelf() {
        posterImageView.sd_setImage(with: movie.imageUrl(size: .w154), placeholderImage: #imageLiteral(resourceName: "moviePoster"))
        titleLabel.text = movie.title
        originalTitleLabel.text = movie.originalTitle
        genresLabel.text = Genre.makeGenresString(movie.genreIDS)
        votesLabel.text = String(movie.voteCount)
        cosmosView.rating = movie.voteAverage / 2
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
