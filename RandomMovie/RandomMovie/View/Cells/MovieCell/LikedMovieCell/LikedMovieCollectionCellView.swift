//
//  LikedMovieCollectionCellView.swift
//  RandomMovie
//
//  Created by  MacBook on 25/03/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit
import Hero

class LikedMovieCollectionViewCell : UICollectionViewCell {    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var lookedBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var posterImageView: UIImageView!
    
    var customCellDelegate: CustomCellDelegate!
    
    var likedMovie : APIModels.LikedMovie! {
        didSet {
            if likedMovie != nil {
                configSelf()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configSelf() {
        if likedMovie.isLooked {
            lookedBarButtonItem.image = #imageLiteral(resourceName: "openEyeIcon")
        } else {
            lookedBarButtonItem.image = #imageLiteral(resourceName: "closeEyeIcon")
        }
        titleLabel.text = likedMovie.title
        posterImageView.sd_setImage(with: likedMovie.imageUrl, placeholderImage: #imageLiteral(resourceName: "noImage"))
    }
    
    @IBAction func deleteMovie(_ sender: UIBarButtonItem) {
        LikedMovies.sharedLikedMovies.removeItem(likedMovie)
    }
    @IBAction func isLookedMovie(_ sender: UIBarButtonItem) {
        likedMovie.isLooked.toggle()
        if likedMovie.isLooked {
            lookedBarButtonItem.image = #imageLiteral(resourceName: "openEyeIcon")
        } else {
            lookedBarButtonItem.image = #imageLiteral(resourceName: "closeEyeIcon")
        }
    }
    @IBAction func moreInfoMovie(_ sender: UIBarButtonItem) {
        let viewController = UINib(nibName: "PreviewMovie", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PreviewMovieViewController
        viewController.movie = likedMovie
        customCellDelegate?.presentViewController?(viewController)
    }
}
