//
//  LoadingTableViewCell.swift
//  RandomMovie
//
//  Created by  MacBook on 24/05/2019.
//  Copyright © 2019 Andrey Bashkirtsev. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoadingTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        let loadingView = NVActivityIndicatorView(frame: self.frame, type: NVActivityIndicatorType.ballRotateChase, color: #colorLiteral(red: 0.09019607843, green: 0.5647058824, blue: 0.2745098039, alpha: 1), padding: 8)
        loadingView.startAnimating()
        
        loadingView.backgroundColor = self.backgroundColor
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        
        let xConstraint = NSLayoutConstraint(item: loadingView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: loadingView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: loadingView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1, constant: 0)
        let heightConstraint = NSLayoutConstraint(item: loadingView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1, constant: 0)
        
        self.addSubview(loadingView)
        
        self.addConstraint(xConstraint)
        self.addConstraint(yConstraint)
        self.addConstraint(widthConstraint)
        self.addConstraint(heightConstraint)
    }
}
